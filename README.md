# TinyWeatherForecastGermany - GitLab Pages index page

[![gitlab ci/cd -> pipeline status](https://gitlab.com/tinyweatherforecastgermanygroup/twfg-repo-mirror-cicd/badges/main/pipeline.svg)](https://gitlab.com/tinyweatherforecastgermanygroup/twfg-repo-mirror-cicd/-/commits/main) [![F-Droid Store release version](https://img.shields.io/f-droid/v/de.kaffeemitkoffein.tinyweatherforecastgermany?color=%23efbb24&logo=fdroid&style=for-the-badge)](https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany) [![code license](https://img.shields.io/github/license/twfgcicdbot/TinyWeatherForecastGermanyMirror?style=for-the-badge&logo=gitlab)](https://gitlab.com/tinyweatherforecastgermanygroup/twfg-repo-mirror-cicd/-/blob/00142082d3e4fc71cc27daf07c5f42c6f9a4ad2e/LICENSE) 

[-> see here for the GitLab Pages page of **this** repository](https://tinyweatherforecastgermanygroup.gitlab.io/index)

[-> see here for a **short summary** page hosted at framagit.org in Germany](https://tinyweatherforecastgermanygroup.frama.io/)

[-> see here for the code **docs**](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/index.html)

**TL;DR** source of the index/main GitLab Pages web site at GitLab of TinyWeatherForecastGermany

## Purpose -> *Why?*

**Target:** quick overview of all content and workflows related to the Project [**Tiny Weather Forecast Germany**](https://codeberg.org/Starfish/TinyWeatherForecastGermany) by Pawel Dube ([@Starfish](https://codeberg.org/Starfish)) at Codeberg, Gitlab and GitHub.

***Note**: 'TWFG' is an inofficial abbreviation for **T**iny **W**eather **F**orecast **G**ermany.*

## Reason -> *What's the added value?*

* increased visibility (e.g. by better [SEO](https://en.wikipedia.org/wiki/Search_engine_optimization) scores) resulting in a broader audience while directing contributors to the *main* repository at *codeberg.org* to keep it as the central location for all activities of the user and dev community
* provide data coverage sources ([`areas.txt`](https://tinyweatherforecastgermanygroup.gitlab.io/index/areas.html), [`stations3.txt`](https://tinyweatherforecastgermanygroup.gitlab.io/index/stations.html)) in a user friendly way (interactive tables and [map](https://tinyweatherforecastgermanygroup.gitlab.io/index/map.html))

## Workflow -> *How does it work?*

**tl;dr** -> it runs completely **autonomously** once per day at 6:05am UTC on shared runners of **GitLab CI/CD**

please see the Gitlab CI/CD pipeline defined in [`.gitlab-ci.yml`](https://gitlab.com/tinyweatherforecastgermanygroup/index/-/blob/a3b9b6676010adb784783a480425f4f53d0d68c6/.gitlab-ci.yml) for details.

## FAQ

### I'd prefer to use GitHub, GitLab, Gitea or framagit instead of codeberg.org to contribute to this project ...

Please see our [list of mirrors](https://tinyweatherforecastgermanygroup.frama.io/#mirrors) to clone the source code from one of them. These are passive mirrors as in **push-only not pull**. To simplify dev work we'd prefer to stay at [codeberg.org](https://codeberg.org/Starfish/TinyWeatherForecastGermany). This is a rather small project run by individuals in their free time. Feel invited to join us at [codeberg.org](https://codeberg.org/user/sing_up) it's free and privacy-friendly!

### Codeberg does not offer a (stable) feature to **search** source code ...

Please see our [list of mirrors](https://tinyweatherforecastgermanygroup.frama.io/#mirrors) and pick [GitHub](https://github.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany), [GitLab](https://gitlab.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany) or [framagit](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanymirror) to search the source code. All of these mirrors offer that.

### I'd like to have a **map** of all locations and areas covered by Tiny Weather Forecast Germany...

Please see [tinyweatherforecastgermanygroup.gitlab.io/index/**map.html**](https://tinyweatherforecastgermanygroup.gitlab.io/index/map.html) for a map visualizing all data sources used by the app.

Copyright of the displayed data -> DWD & WMO. Using maps from different (open) sources (e.g. [Stamen](https://stamen.com/), [OpenTopoMap](https://opentopomap.org)) based on OpenStreetMap and other open data sources -> please see the copyright notice in bottom-right corner.

## License/Copyright and Credits

GNU **G**ENERAL **P**UBLIC **L**ICENSE (**GPLv3**) please see the [`License`](https://gitlab.com/tinyweatherforecastgermanygroup/twfg-repo-mirror-cicd/-/blob/00142082d3e4fc71cc27daf07c5f42c6f9a4ad2e/LICENSE) file for details. Please also see the legal terms noted inline in the code base. Please also make sure to read the privacy and copyright terms of the resources linked above.

**Copyright** of the *main* project -> **Tiny Weather Forecast Germany** -> Pawel Dube ([@Starfish](https://codeberg.org/Starfish))

This CI/CD script was created by Jean-Luc Tibaux (@eUgEntOptIc44)

The **GZIP and brotli compression** feature (high priority for SEO) was integrated using the following GitLab [documentation](https://docs.gitlab.com/ee/user/project/pages/introduction.html#serving-compressed-assets) chapter. 

## Contributing

* As noted above contributions to **Tiny Weather Forecast Germany** are managed at the 'main' code repository
* [**Translations**](https://weblate.bubu1.eu/engage/tiny-weather-forecast-germany/) are managed at the [**weblate** instance](https://weblate.bubu1.eu/projects/tiny-weather-forecast-germany/) provided by Marcus Hoffmann (@Bubu).
* Feel free to contribute to this script by opening issues and/or merge requests.
* Please also see the automatically generated *javadoc* [**code documentation**](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/index.html) of Tiny Weather Forecast Germany [here](https://gitlab.com/tinyweatherforecastgermanygroup/twfg-javadoc).
* For CyberSec, privacy and/or copyright related issues regarding this repository please directly contact the maintainer **Jean-Luc Tibaux** (@eUgEntOptIc44)
