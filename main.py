"""

@title TinyWeatherForecastGermany - Repository overview GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

@url https://tinyweatherforecastgermanygroup.gitlab.io/index/

No warranty or guarantee of any kind provided. Use at your own risk.
Not meant to be used in commercial or in general critical/productive environments at all.

"""

import base64
from datetime import datetime
import hashlib # calculate hashes e.g. sha256
from pathlib import Path
from pprint import pprint
import logging
import json
import shutil
import sys
import random

from bs4 import BeautifulSoup
from dateutil import parser as dtuparser
from dateutil.tz import tzutc # timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
import htmlmin # html minifier
import humanize # human readable file sizes, timestamps, ...

import markdown
from markdown.extensions.toc import TocExtension

from webassets import Bundle, Environment

import requests
import regex

workingDir = Path("public")
workingDir.mkdir(parents=True, exist_ok=True) # create directory if not exists

try:
    logging.basicConfig(format=u'%(asctime)-s %(levelname)s [%(name)s]: %(message)s',
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(Path(workingDir / "debug.log").absolute()), encoding="utf-8"),
            logging.StreamHandler()
    ])
except Exception as e:
    logging.error("while logger init! -> error: "+str(e))

ApiBaseUrl = "https://codeberg.org/api/v1"

ApiRepoUrl = ApiBaseUrl + "/repos/Starfish/TinyWeatherForecastGermany"

ApiReleasesUrl = ApiRepoUrl + "/releases"

ApiContentsUrl = ApiRepoUrl + "/contents"

imageProxyUrl = "https://images.weserv.nl?url=" # WARNING: cdn blocks 'googlebot' user agent -> site won't be listed in google when used for more than local caching !!

imageProxyParams = "&output=webp" # return webp images

imageBaseUrl = imageProxyUrl + "https://codeberg.org/Starfish/TinyWeatherForecastGermany/media/branch/master/"


# sources of user agent data -> License: MIT
#  -> https://github.com/tamimibrahim17/List-of-user-agents/blob/master/Chrome.txt
#  -> https://github.com/tamimibrahim17/List-of-user-agents/blob/master/Firefox.txt
UserAgents = ["Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36","Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2919.83 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2866.71 Safari/537.36","Mozilla/5.0 (X11; Ubuntu; Linux i686 on x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2820.59 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2762.73 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2656.18 Safari/537.36","Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/44.0.2403.155 Safari/537.36","Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36","Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36","Mozilla/5.0 (Windows NT 6.4; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0","Mozilla/5.0 (Windows ME 4.9; rv:31.0) Gecko/20100101 Firefox/31.7","Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20130401 Firefox/31.0","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:28.0) Gecko/20100101 Firefox/31.0","Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20120101 Firefox/29.0","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/29.0","Mozilla/5.0 (X11; OpenBSD amd64; rv:28.0) Gecko/20100101 Firefox/28.0","Mozilla/5.0 (X11; Linux x86_64; rv:28.0) Gecko/20100101  Firefox/28.0","Mozilla/5.0 (Windows NT 6.1; rv:27.3) Gecko/20130101 Firefox/27.3","Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:27.0) Gecko/20121011 Firefox/27.0","Mozilla/5.0 (Windows NT 6.2; rv:20.0) Gecko/20121202 Firefox/26.0","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0","Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:25.0) Gecko/20100101 Firefox/25.0","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0","Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0","Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0","Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/23.0","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:23.0) Gecko/20131011 Firefox/23.0","Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/22.0","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:22.0) Gecko/20130328 Firefox/22.0","Mozilla/5.0 (Windows NT 6.1; rv:22.0) Gecko/20130405 Firefox/22.0","Mozilla/5.0 (Microsoft Windows NT 6.2.9200.0); rv:22.0) Gecko/20130405 Firefox/22.0","Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/21.0.1","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/21.0.1","Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:21.0.0) Gecko/20121011 Firefox/21.0.0","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20130331 Firefox/21.0","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0","Mozilla/5.0 (X11; Linux i686; rv:21.0) Gecko/20100101 Firefox/21.0"]

random.shuffle(UserAgents)

UserAgent = str(random.choice(UserAgents))

logging.debug("querying data as '"+UserAgent+"' ")

apiHeaders = {
    "User-Agent": UserAgent,
    "DNT":"1"
}

oldjs = list(Path(workingDir / "js").glob('script_*.js'))
#print(oldjs)

for oldjsTemp in oldjs:
    try:
        oldjsTemp.unlink()
    except Exception as e:
        logging.error("failed to delete '"+str(oldjsTemp.absolute())+"' -> error: "+str(e))

oldcss = list(Path(workingDir / "css").glob('style_*.css'))
#print(oldcss)

for oldcssTemp in oldcss:
    try:
        oldcssTemp.unlink()
    except Exception as e:
        logging.error("failed to delete '"+str(oldcssTemp.absolute())+"' -> error: "+str(e))

assetsEnv = Environment(directory=str(workingDir.absolute()), url='https://tinyweatherforecastgermanygroup.gitlab.io/index')
js = Bundle('js/luxon.min.js','js/list.min.js','js/script.js', filters='jsmin', output='js/script_%(version)s.min.js')
assetsEnv.register('js_all', js)
css = Bundle('css/style.css', filters='cssmin', output='css/style_%(version)s.min.css')
assetsEnv.register('css_all', css)

def string_to_base64(string, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64encode(string.encode(encoding))

def base64_to_string(base64str, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64decode(base64str).decode(encoding)

def json_rest_req(url_temp):
    """
    JSON REST API requests to urltemp

    returns a python list
    """
    
    url_temp = str(url_temp).strip()
    
    logging.debug("preparing json api request for '"+url_temp+"' ")

    try:
        if len(url_temp) > 0:
            req_temp = requests.get(url_temp, headers=apiHeaders)

            if req_temp.status_code == 200:
                json_temp = json.loads(req_temp.text)

                if len(str(json_temp)) > 4:
                    if json_temp == None:
                        logging.error("content of parsed json api request anwser for '"+url_temp+"' is invalid! -> content: "+str(json_temp))
                        return []

                    return json_temp
                else:
                    logging.error("length of parsed json api request anwser for '"+url_temp+"' is invalid! ")
                    return []
    except Exception as e:
        logging.error("json api request for '"+url_temp+"' failed! -> error: "+str(e))
        return []

repoJson = json_rest_req(ApiRepoUrl)
#print(repoJson)

with open(str(Path(workingDir / "repo-general-info.json").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(json.dumps(repoJson, indent=4)))
#with open(str(Path(workingDir / "repo-general-info.json").absolute()), "r", encoding="utf-8") as fh:
#    repoJson = json.loads(str(fh.read()))

repoTitleShort = str(repoJson["name"])
if repoTitleShort == "None":
    repoTitleShort = ""

repoStarsCount = str(repoJson["stars_count"])
if repoStarsCount == "None":
    repoStarsCount = ""
else:
    repoStarsCount = '<span title="number of users having starred this repository at codeberg" id="repo-stars-count">'+repoStarsCount+' <span id="repo-stars-count-text">Stars</span></span>'

repoForksCount = str(repoJson["forks_count"])
if repoForksCount == "None":
    repoForksCount = ""
else:
    repoForksCount = '<span title="number of users having forked this repository at codeberg" id="repo-forks-count">'+repoForksCount+' <span id="repo-forks-count-text">Forks</span></span>'

repoWatchersCount = str(repoJson["watchers_count"])
if repoWatchersCount == "None":
    repoWatchersCount = ""
else:
    repoWatchersCount = '<span title="number of users watching this repository at codeberg" id="repo-watchers-count">'+repoWatchersCount+' <span id="repo-watchers-count-text">Watchers</span></span>'

releasesJson = json_rest_req(ApiReleasesUrl)
#print(releasesJson)

with open(str(Path(workingDir / "repo-releases-temp.json").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(json.dumps(releasesJson, indent=4)))
#with open(str(Path(workingDir / "repo-releases-temp.json").absolute()), "r", encoding="utf-8") as fh:
#    releasesJson = json.loads(str(fh.read()))

latestReleaseStr = ""
latestReleaseUrlStr = "#"

try:
    if len(releasesJson) > 0:
        latestRelease = releasesJson[0]

        releaseDateTempStr = ""
        releaseDateTemp = str(latestRelease["published_at"])

        releaseAttachmentApkTempStr = ""
        releaseAttachmentApkTemp = list(latestRelease["assets"])

        try:

            if len(releaseAttachmentApkTemp) == 1:
                latestReleaseUrlStr = str(releaseAttachmentApkTemp[0]["browser_download_url"])

            for i in range(len(releaseAttachmentApkTemp)):
                assetTemp = releaseAttachmentApkTemp[i]

                assetNameTemp = str(assetTemp["name"]).strip()
                if ".apk" in assetNameTemp:
                    releaseAttachmentApkTempStr = '<div id="repo-latest-release-asset-'+str(i+1)+'" data-json='+"'"+json.dumps(assetTemp) + "'" + ' data-id="'+str(assetTemp["id"])+'"><a id="latest-release-apk-download-link" title="download latest release apk" href="'+str(assetTemp["browser_download_url"])+'"><span id="latest-release-apk-filename">'+assetNameTemp+'</span>&nbsp;&nbsp;&nbsp;<span id="latest-release-apk-size" data-size="'+str(assetTemp["size"])+'">('+str(humanize.naturalsize(assetTemp["size"], binary=True))+')</span>&nbsp;&nbsp;&nbsp;<span id="latest-release-apk-download-count" data-downloadcount="'+str(assetTemp["download_count"])+'">'+str(assetTemp["download_count"])+' Downloads</span></a></div>'
        except Exception as e:
            logging.error("failed to parse repository release assets -> error: "+str(e))

        try:
            releaseDateTempStr = 'at '+str(dtuparser.parse(releaseDateTemp).strftime("%Y-%m-%d %H:%M:%S")) # e.g. 2021-07-09T18:40:59+02:00
        except Exception as e:
            logging.error("failed to parse repository release date -> error: "+str(e))

        latestReleaseStr = '<div id="repo-latest-release-container" aria-live="polite" data-id="'+str(latestRelease["id"])+'"><strong>Changelog </strong> \n<span class="repo-latest-release-title">'+str(latestRelease["name"])+'</span> published <span class="repo-latest-release-date" data-timestamp="'+releaseDateTemp+'">'+releaseDateTempStr+'</span>\n<div class="repo-latest-release-body">'+str(markdown.markdown(str(latestRelease["body"]), extensions=['extra', 'sane_lists', TocExtension(baselevel=3, title='Table of contents', anchorlink=True)]))+'</div>'+releaseAttachmentApkTempStr+'</div>'
    
    else:
        logging.error("failed to get repository releases -> result length is invalid!")
except Exception as e:
    logging.error("failed to parse repository release data -> error: "+str(e))

readmeFileJson = json_rest_req(ApiContentsUrl + "/README.md")
#print(readmeFileJson)

cssAssetUrlsStr = ""
cssAssetUrls = list(assetsEnv['css_all'].urls())
for cssAssetUrlTemp in cssAssetUrls:
    cssAssetUrlsStr += '<link rel="stylesheet" href="' + str(cssAssetUrlTemp) + '">\n'

robotsTXT = """
User-agent: *
Allow: /

Sitemap: https://tinyweatherforecastgermanygroup.gitlab.io/index/sitemap.xml
"""

with open(str(Path(workingDir / "robots.txt").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(robotsTXT))

lastModPageStrSiteMap = ""
try:
    lastModPageStrSiteMap = '<lastmod>'+str(datetime.now(tzutc()).strftime("%Y-%m-%d"))+'</lastmod>' # "%Y-%m-%dT%H:%M+00:00"
except Exception as e:
    logging.error("failed to generate meta tag 'pubdate' -> error: "+str(e))

sitemapXML = """<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
 <url>
   <loc>https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html</loc>
   """+lastModPageStrSiteMap+"""
   <changefreq>daily</changefreq>
 </url>
 <url>
   <loc>https://tinyweatherforecastgermanygroup.gitlab.io/index/areas.html</loc>
   """+lastModPageStrSiteMap+"""
   <changefreq>daily</changefreq>
 </url>
 <url>
   <loc>https://tinyweatherforecastgermanygroup.gitlab.io/index/stations.html</loc>
   """+lastModPageStrSiteMap+"""
   <changefreq>daily</changefreq>
 </url>
</urlset>
"""

with open(str(Path(workingDir / "sitemap.xml").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(sitemapXML))

pubDateStr = ""
try:
    pubDateStr = '<meta name="pubdate" content="'+str(datetime.now(tzutc()).strftime("%Y%m%d"))+'">'
except Exception as e:
    logging.error("failed to generate meta tag 'pubdate' -> error: "+str(e))

htmlResultStr = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Tiny Weather Forecast Germany - Kleine Wettervorschau Deutschland</title>

    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    <link rel="manifest" href="images/site.webmanifest">
    <link rel="mask-icon" href="images/safari-pinned-tab.svg" color="#293235">
    <link rel="shortcut icon" href="images/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Tiny Weather Forecast Germany">
    <meta name="application-name" content="Tiny Weather Forecast Germany">
    <meta name="msapplication-TileColor" content="#293235">
    <meta name="msapplication-config" content="images/browserconfig.xml">
    <meta name="theme-color" content="#293235">

    """+cssAssetUrlsStr+"""

    <meta name="description" content="Tiny Weather Forecast Germany - android app using open weather data by DWD">
    <meta name="keywords" content="DWD, Deutscher Wetterdienst, android, app, open source, weather, wetter, rainradar, regenradar, map, charts, open data, germany, deutschland, allemagne, duitsland, météo" />
    <meta name="robots" content="index, follow">
    <meta name="audience" content="all">
    <meta name="thumbnail" content="images/icon.png">
    <meta name="revisit-after" content="2 days">
    <meta name="google-site-verification" content="MdTS4FWQKzHI34uGnZwU87K2sivqjVQjGe3E-yGd09Y" />
    """+pubDateStr+"""

    <link rel="dns-prefetch" href="https://weblate.bubu1.eu/" >
    <link rel="preconnect" href="https://weblate.bubu1.eu/" crossorigin>

    <meta property="og:title" content="Tiny Weather Forecast Germany - android app using DWD open weather data">
    <meta property="og:description" content="TinyWeatherForecastGermany is an open source android app using open weather data provided by Deutscher Wetterdienst (DWD)">
    <meta property="og:image" content="images/twfg-repository-open-graph-graphic.png">
    <meta property="og:url" content="https://tinyweatherforecastgermanygroup.gitlab.io/index/">

    <meta name="twitter:title" content="Tiny Weather Forecast Germany - android app using DWD open weather data">
    <meta name="twitter:description" content="TinyWeatherForecastGermany is an open source android app using open weather data provided by Deutscher Wetterdienst (DWD)">
    <meta name="twitter:image" content="images/twfg-repository-open-graph-graphic.png">
    <meta name="twitter:card" content="summary_large_image">
   
    <script type="application/ld+json">
    {
        "@context" : "http://schema.org",
        "@type" : "MobileApplication",
        "applicationCategory" : "Weather",
        "genre" : "Weather",
        "name" : "Tiny Weather Forecast Germany",
        "description" : "Weather forecast based on open data from the Deutscher Wetterdienst (DWD).",
        "operatingSystem" : "Android 4.4+",
        "installUrl" : "https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany/",
        "image" : "https://tinyweatherforecastgermanygroup.gitlab.io/index/images/icon.png",
        "screenshot" : "https://f-droid.org/repo/de.kaffeemitkoffein.tinyweatherforecastgermany/en-US/phoneScreenshots/1.png",
        "creator" : {
            "@context" : "http://schema.org",
            "@type" : "Person",
            "name" : "Pawel Dube (Starfish)",
            "url" : "https://codeberg.org/Starfish"
        },
        "sourceOrganization" : {
            "@context" : "http://schema.org",
            "@type" : "Organization",
            "name" : "TinyWeatherForecastGermanyGroup",
            "url" : "https://gitlab.com/tinyweatherforecastgermanygroup"
            },
        "offers" : {
            "@type" : "Offer",
            "url" : "https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany/",
            "price": 0,
            "priceCurrency" : "EUR"
        }
    }
    </script>

</head>
<body>
    <article>

    <div id="page-container" class="container" role="main">

    <div id="repo-metadata-container" role="contentinfo">
        <a href='"""+latestReleaseUrlStr+"""'>
            <svg role="img" alt="app icon" title="Tiny Weather Forecast Germany app icon" id="repo-avatar-img" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 238.37 237.426"><path fill="#fbfbfc" d="M92.201 142.035c-3.04-.586-1.712-1.223 9.356-6.847 29.683-15.083 30.129-21.85 2.213-44.148-17.48-13.963-36.478-15.755-8.837-19.806 5.9-.864 7.55-.049 5.646 1.918-3.332 3.443-1.024 6.344 14.689 19.055 26.228 21.217 25.773 28.05-2.794 41.984-6.806 3.32-12.91 6.742-12.475 7.259 2.53 3.013-3.859 1.343-7.798.585zm-38.12-71.978c-14.328-5.797 6.89-35.755 28.394-39.795 5.106-.959 8.144.154 9.897 3.027 3.548 5.82 1.272 9.293-8.833 13.482-8.461 3.507-11.953 6.344-17.703 14.608-7.27 10.449-8.22 10.109-11.756 8.678z"/><path fill="#a5c6f7" d="M99.784 142.517c-.905-1.465.47-2.365 12.78-8.37 28.466-13.885 28.908-20.742 2.704-41.94-15.713-12.71-12.343-14.808-14.182-18.775 0 0 1.76-1.176-6.933-.424-39.089 3.38-54.414 12.789-65.323 25.086-4.04 4.555-4.75 3.012-4.81 2.131-.295-4.446.134-2.801 1.007-8.874 4.839-33.686 37.133-66.37 66.092-73.88 25.615-6.64 33.459-12.159 30.824-1.23-5.645 23.417 19.73 45.605 42.421 34.124 8.12-4.108 13.827-16.135 15.454-14.684 3.117 2.779 8.902 7.985 14.755 14.119 9.78 10.248 16.04 33.271 19.068 46.711 3.21 14.25-3.408 1.45-7.363-2.627-14.27-14.26-43.143-22.61-79.308-22.656-31.113-.04-33.002 2.125-14.597 16.719 31.198 24.737 31.3 32.756.61 47.598-6.935 3.354-11.93 6.431-11.229 6.595 10.85 2.545-1.238 1.562-1.97.377zM60.242 68.575c.788-.693 3.306-3.931 5.594-7.196 5.779-8.244 9.242-11.101 17.703-14.608 10.105-4.189 12.38-7.663 8.833-13.482-7.242-11.876-42.888 12.556-42.888 29.396 0 6.63 6.11 9.976 10.758 5.89z"/><path fill="#f3dd88" d="M112.407 143.544c3.143-.15 8.466.603 11.608.752 3.142.15.571.272-5.713.272s-9.037-.875-5.895-1.024zm-20.109-.232c-1.517-.33-7.545-1.56-13.396-2.734-68.796-13.798-81.967-49.534-4.028-65.384 12.214-2.484 14.27-2.61 14.277-.248.01 3.472 2.493 6.06 16.543 17.244 26.054 20.74 25.487 28.839-3.953 43.291-12.079 5.93-12.586 6.016-7.276 6.65 18.124 2.165 11.116 1.862 5.906 1.855-3.408.217-6.556-.344-8.073-.674zM177.64 40.364c7.053-7.197-.112-6.523 3.109-3.806-1.23-1.082-4.568 4.55-3.109 3.806z"/><path fill="#efbb24" d="M109.594 193.697c-39.177-1.117-60.725-18.303-77.027-43.367-7.184-11.046-13.952-45.042-6.145-35.25 14.734 18.478 36.502 21.199 47.436 23.504 13.322 2.808 20.047 3.706 26.199 4.415 7.828.902 33.728.565 44.787-1.018 31.426-4.497 51.53-10.743 62.699-22.52 5.258-5.543 8.983-32.322 5.417 4.227-5.213 53.43-45.758 71.652-103.366 70.01zm30.597-140.846c-13.126-4.102-20.319-18.915-19.288-29.878 1.106-11.772-.589-12.094 9.32-10.027 13.293 2.773 27.888 7.354 42.165 17.279 10.777 7.492 9.965 4.367 4.446 10.951-9.541 11.382-25.428 15.18-36.643 11.675z"/><path fill="#4e86e6" d="M103.057 143.307c-5.505-.533-5.482-2.12 8.94-8.904 30.287-14.245 29.343-21.42-1.165-45.502C94.417 75.944 92.847 68.96 123.96 69.466c36.378.592 68.656 10.316 83.222 25.085 2.771 2.809 3.248 5.315 5.833 7.683 2.655 2.432 1.636-5.035 1.609-.296-.03 4.923-2.256 4.16-4.475.807-9.972-15.07-68.1-36.083-86.938-29.516-6.564 2.288-4.742 4.48 16.367 19.688 23.37 16.838 23.974 25.059 2.952 40.164-8.81 6.33-9.903 9.123-3.74 9.548 1.084.075-.512.391-3.546.703-5.733.588-22.47.915-28.968.286z"/><path fill="#3865cc" d="M135.565 142.08c-2.123-1.346-.178-3.866 6.965-8.999 21.022-15.105 20.418-23.326-2.952-40.164-21.11-15.209-24.987-20.043-18.423-22.331 18.814-6.559 77.13 9.36 89.538 27.827 6.211 10.153 3.184 2.608 4.67 4.684 7.657 11.274-15.38 26.629-40.663 33.898-15.684 4.51-36.04 7.046-39.135 5.085z"/><path fill="#2c9f5f" d="M100.966 215.33c-52.601-6.1-81.359-24.804-58.92-46.025 4.415-4.177 3.506-3.702 8.698 1.676 15.179 15.724 35.097 22.218 67.952 22.154 33.032-.064 51.757-7.502 68.873-22.436 5.412-4.722 3.943-5.89 8.455-1.272 17.755 18.17-2.54 36.803-46.99 44.44-7.171 1.232-41.143 2.265-48.068 1.462z"/><path fill="#293235" d="M106.482 231.926c-2.87-.19 0 0-9.717-1.935-47.844-9.188-81.139-45.249-90.648-98.18-2.382-13.259-1.018-26.753 4.716-46.635C26.013 32.54 85.183-3.944 137.752 6.92c52.563 10.863 84.497 44.392 94.311 99.024 2.106 11.717 1.818 16.958-1.966 35.785-8.41 41.85-41.45 76.117-82.55 85.85-19.944 4.722-22.807 5.563-41.065 4.347zm42.552-18.06c44.766-7.69 65.271-27.786 46.209-45.285-2.696-2.475-3.995-1.377 1.489-8.763 37.486-50.495 9.98-123.4-49.42-141.454C62.001-7.562-11.723 88.507 41.53 159.94c6.436 8.634 4.547 5.018 1.054 9.025-17.145 19.67 5.781 40.264 58.382 46.364 6.925.803 40.897-.23 48.068-1.462z"/></svg>
        </a>
        <h1 role="heading" id="repo-title-heading"><span id="repo-title-heading-short">Tiny Weather Forecast Germany</span><span id="repo-title-heading-long">&nbsp;- open source android weather app</span></h1>
        
        <div id="repo-metadata-stats" role="status" aria-live="polite">
            """+repoStarsCount+"""
            """+repoForksCount+"""
            """+repoWatchersCount+"""
        
            <span role="link" id="repo-code-docs">
                <a href="https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/index.html" title="javadoc code documentation">docs</a>
            </span>
            <a role="complementary" aria-label="link to weblate page with translations of this app" id="repo-translation-badge" href="https://weblate.bubu1.eu/engage/tiny-weather-forecast-germany/" title="Translate Tiny Weather Forecast Germany at Weblate">
                <img src="https://weblate.bubu1.eu/widgets/tiny-weather-forecast-germany/-/svg-badge.svg" alt="weblate live translation status" title="weblate translation status" />
            </a>
        </div>

        <div id="repository-platforms" role="group">
            <a tabindex="1" href="https://codeberg.org/Starfish/TinyWeatherForecastGermany" title="the main repository at codeberg.org"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 4.2150375 4.2150376" height="40px" width="40px"> <defs> <linearGradient gradientUnits="userSpaceOnUse" y2="-6966.9307" x2="42575.336" y1="-7078.7891" x1="42519.285" id="linearGradient6918" xlink:href="#linearGradient6924" /> <linearGradient id="linearGradient6924"> <stop offset="0" style="stop-color:#2185d0;stop-opacity:0" /> <stop style="stop-color:#2185d0;stop-opacity:0.48923996" offset="0.49517274" id="stop6926" /> <stop offset="1" style="stop-color:#2185d0;stop-opacity:0.63279623" /> </linearGradient> </defs> <g transform="translate(-44.876583,-76.12794)"> <ellipse style="fill:#ffffff;fill-opacity:0.98991939;stroke-width:0.26458332" id="path4524" cx="46.9841" cy="78.235458" rx="2.1075189" ry="2.1075187" /> <g transform="matrix(0.09136496,0,0,0.09136496,-980.82243,248.23174)" id="g6933"> <path id="path6733" style="opacity:0.5;vector-effect:none;fill:url(#linearGradient6918);fill-opacity:1;stroke:none;stroke-width:3.67845988;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:2;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke markers fill" d="m 42519.285,-7078.7891 a 0.76086879,0.56791688 0 0 0 -0.738,0.6739 l 33.586,125.8886 a 87.182358,87.182358 0 0 0 39.381,-33.7636 l -71.565,-92.5196 a 0.76086879,0.56791688 0 0 0 -0.664,-0.2793 z" transform="scale(0.26458333)" /> <path transform="scale(0.26458333)" id="circle6810" style="opacity:1;fill:#2185d0;fill-opacity:1;stroke:none;stroke-width:2.01452994px;stroke-opacity:1;paint-order:stroke markers fill" d="m 42517.32,-7119.4805 a 87.182358,87.182358 0 0 0 -86.855,87.1817 87.182358,87.182358 0 0 0 13.312,46.3047 l 72.688,-93.9727 a 1.3609979,1.0158567 0 0 1 2.359,0 l 72.692,93.9766 a 87.182358,87.182358 0 0 0 13.314,-46.3086 87.182358,87.182358 0 0 0 -87.182,-87.1817 87.182358,87.182358 0 0 0 -0.328,0 z" /> </g> </g></svg></a>

            <a tabindex="2" href="https://gitlab.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany" title="mirror repository at gitlab.com"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 210 194"><g fill="none" fill-rule="evenodd"><path fill="#E24329" d="m105.061 193.655 38.64-118.921h-77.28l38.64 118.921Z"/><path fill="#FC6D26" d="M105.061 193.655 66.421 74.734H12.268l92.793 118.92Z"/><path fill="#FCA326" d="M12.268 74.734.526 110.874a8 8 0 0 0 2.907 8.943l101.628 73.838-92.793-118.92Z"/><path fill="#E24329" d="M12.268 74.734h54.153L43.148 3.11c-1.197-3.686-6.41-3.685-7.608 0L12.27 74.734Z"/><path fill="#FC6D26" d="m105.061 193.655 38.64-118.921h54.153l-92.793 118.92Z"/><path fill="#FCA326" d="m197.854 74.734 11.742 36.14a8 8 0 0 1-2.906 8.943l-101.629 73.838 92.793-118.92Z"/><path fill="#E24329" d="M197.854 74.734h-54.153L166.974 3.11c1.197-3.686 6.411-3.685 7.608 0l23.272 71.625Z"/></g></svg></a>

            <a tabindex="3" href="https://github.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany" title="mirror repository at github.com"><svg id="github-svg-icon" xmlns="http://www.w3.org/2000/svg" width="40" viewBox="0 0 24 24"><path d="M12 .297c-6.63 0-12 5.373-12 12 0 5.303 3.438 9.8 8.205 11.385.6.113.82-.258.82-.577 0-.285-.01-1.04-.015-2.04-3.338.724-4.042-1.61-4.042-1.61C4.422 18.07 3.633 17.7 3.633 17.7c-1.087-.744.084-.729.084-.729 1.205.084 1.838 1.236 1.838 1.236 1.07 1.835 2.809 1.305 3.495.998.108-.776.417-1.305.76-1.605-2.665-.3-5.466-1.332-5.466-5.93 0-1.31.465-2.38 1.235-3.22-.135-.303-.54-1.523.105-3.176 0 0 1.005-.322 3.3 1.23.96-.267 1.98-.399 3-.405 1.02.006 2.04.138 3 .405 2.28-1.552 3.285-1.23 3.285-1.23.645 1.653.24 2.873.12 3.176.765.84 1.23 1.91 1.23 3.22 0 4.61-2.805 5.625-5.475 5.92.42.36.81 1.096.81 2.22 0 1.606-.015 2.896-.015 3.286 0 .315.21.69.825.57C20.565 22.092 24 17.592 24 12.297c0-6.627-5.373-12-12-12"/></svg></a>

            <a tabindex="4" href="https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany" title="TinyWeatherForecastGermany in the F-Droid app store"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="45" width="50">  <defs>    <path id="c" fill="#fff" fill-opacity=".298" d="M2.6113 1005.6094c-.4534.012-.7617.189-.9807.4486 2.027 2.4089 2.3684 2.7916 5.1355 6.2214 1.0195 1.319 2.0816.6373 1.062-.6817l-4.25-5.5c-.229-.3056-.585-.478-.9668-.4883z" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1"/>    <path id="d" fill="#263238" fill-opacity=".2" d="M1.6221 1006.0705c-.1239.1479-.5612.8046-.0225 1.5562l4.25 5.5c1.0195 1.319 1.1499-.6123 1.1499-.6123s-3.7345-4.51-5.3774-6.4439z" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1"/>    <path id="e" fill="#8ab000" d="M2.3379 1005.8443c-.4382 0-.9599.1416-.8242.7986.1035.5016 4.6608 6.0744 4.6608 6.0744 1.0195 1.319 2.4935.6763 1.474-.6425l-4.2344-5.4727c-.2603-.29-.6086-.7436-1.0762-.7578z" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1"/>  </defs>  <defs>    <radialGradient xlink:href="#a" id="f" cx="-98.2338" cy="3.4696" r="22.6712" fx="-98.2338" fy="3.4696" gradientTransform="matrix(0 1.97476 -2.11722 0 8.6772 1199.588)" gradientUnits="userSpaceOnUse"/>    <linearGradient id="a">      <stop offset="0" stop-color="#fff" stop-opacity=".098"/>      <stop offset="1" stop-color="#fff" stop-opacity="0"/>    </linearGradient>    <filter id="b" width="1.0477" height="1.0483" x="-.0238" y="-.0242" color-interpolation-filters="sRGB">      <feGaussianBlur stdDeviation=".4505"/>    </filter>  </defs>  <path fill="#263238" fill-opacity=".4" fill-rule="evenodd" d="M2.6135 1006.3488a1.2501 1.2501 0 0 0-1.0118 2.0293l3.6036 4.6641c-.127.3331-.2032.6915-.2032 1.0703v7c0 1.662 1.338 3 3 3h32c1.662 0 3-1.338 3-3v-7c0-.3803-.077-.74-.205-1.0742l3.6015-4.6602a1.2501 1.2501 0 0 0-1.0488-2.0273 1.2501 1.2501 0 0 0-.9297.498l-3.4316 4.4414c-.3103-.1079-.6384-.1777-.9864-.1777h-32c-.3485 0-.6775.069-.9882.1777l-3.4336-4.4414a1.2501 1.2501 0 0 0-.9668-.5zm5.3886 18.7637c-.2077 0-.4098.021-.6054.061-1.3695.2761-2.3946 1.4698-2.3946 2.9101v20.0287c0 1.662 1.338 3 3 3h32c1.662 0 3-1.338 3-3v-20.029c0-1.4403-1.025-2.634-2.3945-2.9101a3.0919 3.0919 0 0 0-.6055-.061h-32z" color="#000" enable-background="accumulate" filter="url(#b)" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1" transform="translate(0 -1004.3622)"/>  <g fill-rule="evenodd">    <path fill="#8ab000" stroke="#769616" stroke-linecap="round" stroke-width="2.5" d="m45.4108 2.5-4.25 5.5"/>    <use xlink:href="#c" fill-opacity=".298" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1" transform="matrix(-1 0 0 1 47.9998 -1004.3622)"/>    <use xlink:href="#d" fill-opacity=".2" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1" transform="matrix(-1 0 0 1 47.9998 -1004.3622)"/>    <use xlink:href="#e" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1" transform="matrix(-1 0 0 1 47.9998 -1004.3622)"/>  </g>  <g fill-rule="evenodd">    <path fill="#8ab000" stroke="#769616" stroke-linecap="round" stroke-width="2.5" d="M2.589 2.5 6.839 8"/>    <use xlink:href="#c" fill-opacity=".298" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1" transform="translate(0 -1004.3622)"/>    <use xlink:href="#d" fill-opacity=".2" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1" transform="translate(0 -1004.3622)"/>    <use xlink:href="#e" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1" transform="translate(0 -1004.3622)"/>  </g>  <g transform="translate(42 -1004.3622)">    <rect width="38" height="13" x="-37" y="1010.3622" fill="#aeea00" rx="3" ry="3"/>    <rect width="38" height="10" x="-37" y="1013.3622" fill="#263238" fill-opacity=".2" rx="3" ry="3"/>    <rect width="38" height="10" x="-37" y="1010.3622" fill="#fff" fill-opacity=".298" rx="3" ry="3"/>    <rect width="38" height="11" x="-37" y="1011.3622" fill="#aeea00" rx="3" ry="2.5385"/>  </g>  <g transform="translate(0 -1004.3622)">    <rect width="38" height="26" x="5" y="1024.3622" fill="#1976d2" rx="3" ry="3"/>    <rect width="38" height="13" x="5" y="1037.3622" fill="#263238" fill-opacity=".2" rx="3" ry="3"/>    <rect width="38" height="13" x="5" y="1024.3622" fill="#fff" fill-opacity=".2" rx="3" ry="3"/>    <rect width="38" height="24" x="5" y="1025.3622" fill="#1976d2" rx="3" ry="2.7692"/>  </g>  <g transform="translate(0 9)">    <path fill="#0d47a1" d="M24 17.75c-2.8807 0-5.3198 1.9847-6.0332 4.6504h3.2129C21.734 21.415 22.7748 20.75 24 20.75c1.8127 0 3.25 1.4373 3.25 3.25s-1.4373 3.25-3.25 3.25c-1.3074 0-2.4113-.7527-2.9297-1.8496H17.916C18.5583 28.166 21.048 30.25 24 30.25c3.434 0 6.25-2.816 6.25-6.25s-2.816-6.25-6.25-6.25z" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1"/>    <circle cx="24" cy="24" r="9.55" fill="none" stroke="#0d47a1" stroke-linecap="round" stroke-width="1.9"/>  </g>  <g transform="translate(0 -1003.8622)">    <ellipse cx="14.375" cy="1016.4872" fill="#263238" fill-opacity=".2" rx="3.375" ry="3.875"/>    <circle cx="14.375" cy="1016.9872" r="3.375" fill="#fff"/>  </g>  <g transform="translate(19.5 -1003.8622)">    <ellipse cx="14.375" cy="1016.4872" fill="#263238" fill-opacity=".2" rx="3.375" ry="3.875"/>    <circle cx="14.375" cy="1016.9872" r="3.375" fill="#fff"/>  </g>  <path fill="url(#f)" fill-rule="evenodd" d="M2.6135 1005.5987a1.2501 1.2501 0 0 0-1.0118 2.0293l3.6036 4.6641c-.127.3331-.2032.6915-.2032 1.0703v7c0 1.662 1.338 3 3 3h32c1.662 0 3-1.338 3-3v-7c0-.3803-.077-.74-.205-1.0742l3.6015-4.6602a1.2501 1.2501 0 0 0-1.0488-2.0273 1.2501 1.2501 0 0 0-.9297.498l-3.4316 4.4414c-.3103-.1079-.6384-.1777-.9864-.1777h-32c-.3485 0-.6775.069-.9882.1777l-3.4336-4.4414a1.2501 1.2501 0 0 0-.9668-.5zm5.3886 18.7637c-.2077 0-.4098.021-.6054.061-1.3695.2761-2.3946 1.4698-2.3946 2.9101v20.0287c0 1.662 1.338 3 3 3h32c1.662 0 3-1.338 3-3v-20.029c0-1.4403-1.025-2.634-2.3945-2.9101a3.0919 3.0919 0 0 0-.6055-.061h-32z" color="#000" enable-background="accumulate" font-family="sans-serif" font-weight="400" overflow="visible" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1" transform="translate(0 -1004.3622)"/></svg></a>
        </div>
    </div>

    """+latestReleaseStr+"""

    <div tabindex="5" role="article" id="readme-content-container" aria-label="README.md file contents">
    <strong>README.md</strong>

"""

with open(str(Path(workingDir / "repo-readme-file.json").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(json.dumps(readmeFileJson, indent=4)))
#with open(str(Path(workingDir / "repo-readme-file.json").absolute()), "r", encoding="utf-8") as fh:
#    readmeFileJson = json.loads(str(fh.read()))

readmeFileContent = str(readmeFileJson["content"])
readmeFileEncoding = str(readmeFileJson["encoding"]).strip()

if readmeFileEncoding == "base64":
    readmeFileContent = str(base64_to_string(readmeFileJson["content"]))
else:
    logging.warning("detected unexpected file encoding '"+readmeFileEncoding+"' of 'README.md' ("+str(readmeFileJson["url"])+") ")

readmeFileContent = readmeFileContent.replace(u'\ufeff', '') # remove invisible char Byte order mark -> https://en.wikipedia.org/wiki/Byte_order_mark

with open(str(Path(workingDir / "repo-readme-file.md").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(readmeFileContent)
#with open(str(Path(workingDir / "repo-readme-file.md").absolute()), "r", encoding="utf-8") as fh:
#    readmeFileContent = str(fh.read())

if len(readmeFileContent) > 5:
    readmeFileContent = "\n\n[TOC]\n\n" + readmeFileContent # add placeholder for table of contents 

readmeFileHtml = markdown.markdown(readmeFileContent, extensions=['extra', 'sane_lists', TocExtension(baselevel=2, title='Table of contents', anchorlink=True, toc_depth="3-5")]) # 'nl2br',

readmeFileSoup = BeautifulSoup(readmeFileHtml, features='html.parser') # parse html to modify elements

readmeFileImg = readmeFileSoup.select("img")

try:
    for imgIndex in range(len(readmeFileImg)): # images in README.md
        imgTemp = readmeFileImg[imgIndex]

        imgSrcTemp = str(imgTemp.get('src')).strip()
        imgLoadingTemp = str(imgTemp.get('loading')).strip()
        imgClassListTemp = str(imgTemp.get('class')).strip()

        if 'weblate' in imgSrcTemp:
            logging.debug('skipping image "'+str(imgTemp)+'" -> weblate live badge')
            continue

        imgAltTemp = str(imgTemp.get('alt')).strip()
        imgTitleTemp = str(imgTemp.get('title')).strip()

        imgWidthTemp = str(imgTemp.get('width')).strip()
        imgHeightTemp = str(imgTemp.get('height')).strip()

        if len(imgSrcTemp) < 5:
            imgTemp.decompose()
            logging.warning("removed image '"+imgSrcTemp+"' from README.md content -> 'src' is invalid!")
        else:
            if "http" not in imgSrcTemp:
                imgTemp['src'] = imageBaseUrl + imgSrcTemp + imageProxyParams
            else:
                imgTemp['src'] = imageProxyUrl + imgSrcTemp + imageProxyParams
            
            strippedSrcTemp = str(imgTemp['src'])

            try:
                if imgLoadingTemp.lower() == "none":
                    imgTemp['loading'] = 'lazy' # adding lazy loading to images from README.md -> docs: https://developer.mozilla.org/en-US/docs/Web/Performance/Lazy_loading#images_and_iframes -> also see: https://caniuse.com/loading-lazy-attr -> WARNING: as of july 2021 only partially supported!
            except Exception as e:
                logging.error("failed to set the 'loading' attribute of image '" + strippedSrcTemp + "' -> error: "+str(e))

            try:
                if len(imgAltTemp) < 5:
                    imgTemp['alt'] = "image "+str(imgIndex+1)+" located at " + strippedSrcTemp.replace(imageProxyUrl,'').replace(imageProxyParams,'')
            except Exception as e:
                logging.error("failed to set the 'alt' attribute of image '" + strippedSrcTemp + "' -> error: "+str(e))

            try:
                if len(imgTitleTemp) < 5:
                    imgTemp['title'] = "image "+str(imgIndex+1)+" located at " + strippedSrcTemp.replace(imageProxyUrl,'').replace(imageProxyParams,'')
            except Exception as e:
                logging.error("failed to set the 'title' attribute of image '" + strippedSrcTemp + "' -> error: "+str(e))

            # for the F-Droid badge
            try:
                if "badge" in str(imgTitleTemp).lower() or "badge" in str(imgSrcTemp).lower():
                    imgTemp['class'] = str(imgClassListTemp).lower().replace("none","") + " supplemental badge-img" # to hide from browser 'reader mode'
                    imgTemp['class'] = str(imgTemp.get('class')).strip()
                    imgTemp['style'] = 'max-width: 200px!important;height:auto;'
                    imgTemp['role'] = 'complementary'
                    imgTemp['aria-label'] = 'F-Droid store page download link'
            except Exception as e:
                logging.error("failed to search 'title' and 'href' attributes of image '" + strippedSrcTemp + "' for 'badge' -> error: "+str(e))

            try:
                if ".png" in str(imgTemp['src']) or ".jpg" in str(imgTemp['src']):
                    from PIL import Image
                        
                    fileHashTemp = ""
                    try:
                        fileHashTemp = hashlib.sha256(str(strippedSrcTemp).encode("utf-8")).hexdigest()
                    except Exception as e:
                        logging.error("failed to caluclate hash for url '"+str(strippedSrcTemp)+"' -> error: "+str(e))
                        fileHashTemp = regex.sub(r"[^A-z0-9\.\-\_]","",str(strippedSrcTemp))

                    downloadFileReq = requests.get(str(imgTemp['src']), stream=True, headers=apiHeaders)

                    downloadFileSuffix = '.png'
                    if "=webp" in str(imgTemp['src']):
                        downloadFileSuffix = '.webp'
                    downloadFilePath = Path(workingDir / "images") / str(str(fileHashTemp)+downloadFileSuffix)

                    logging.debug(str(downloadFilePath))

                    with open(str(downloadFilePath.absolute()), 'wb') as out_file:
                        shutil.copyfileobj(downloadFileReq.raw, out_file)
                    del downloadFileReq # free space in memory

                    imageObjectTemp = Image.open(downloadFilePath)
                    imageObjectWidth, imageObjectHeight = imageObjectTemp.size

                    if type(imageObjectWidth) == int:
                        if "mipmap-mdpi" not in str(imgTemp['title']):
                            imgTemp['width'] = imageObjectWidth
                    else:
                        logging.error(" while processing image '"+str(imgTemp['src'])+"': ")
                        logging.error("data type '"+str(type(imageObjectWidth))+"' of 'imageObjectWidth' -> '"+str(imageObjectWidth)+"' is invalid the attribute 'width' will be skipped! ")
                        
                    if type(imageObjectHeight) == int:
                        if "mipmap-mdpi" not in str(imgTemp['title']):
                            imgTemp['height'] = imageObjectHeight
                    else:
                        logging.error(" while processing image '"+str(imgTemp['src'])+"': ")
                        logging.error("data type '"+str(type(imageObjectHeight))+"' of 'imageObjectHeight' -> '"+str(imageObjectHeight)+"' is invalid the attribute 'width' will be skipped! ")

                    imgTemp['src'] = "https://tinyweatherforecastgermanygroup.gitlab.io/index/images/"+str(fileHashTemp)+downloadFileSuffix

            except Exception as e:
                logging.error("failed to process the 'width' attribute of image '" + strippedSrcTemp + "' -> error: "+str(e))
except Exception as e:
    logging.error("while processing images in README.md -> error: "+str(e))

htmlResultStr += str(readmeFileSoup)

htmlResultStr += "\n\t</div>"

htmlResultStr += """
    <div id="copyright" role="complementary" aria-label="Copyright information">
        <h3 id="copyright-heading">Copyright</h3>
        <p>Used trademarks are under copyright of their creators and/or owners.</p>
        <p>Please also see the project-specific copyright notice above</p>
        <ul>
            <li>The <strong>TinyWeatherForecastGermany</strong> icon was created by Janis Bitta</li>
            <li>The <strong>Codeberg</strong> icon licensed under <a href="http://creativecommons.org/publicdomain/zero/1.0/">CC0 1.0</a> was created by <a href="https://codeberg.org/mray">@mray</a></li>
            <li>The <strong>GitLab</strong> icon was retrieved from the <a href="https://about.gitlab.com/press/press-kit/">GitLab Press Kit</a> and is under the copyright of <a href="https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/trademark-guidelines/">GitLab Inc.</a></li>
            <li>The <strong>GitHub</strong> icon was retrieved from <a href="https://github.com/simple-icons/simple-icons">simple-icons</a> and is under the copyright of GitHub Inc.</li>
            <li>The <strong>F-Droid</strong> (2015 edition) store icon is dual-licensed under CC-BY-SA 3.0 Unported and GPLv3 or later versions of that license. The icon was retrieved from <a href="https://de.wikipedia.org/wiki/Datei:F-Droid_Logo_4.svg">Wikipedia</a> it has been created by William Theaker, Robert Martinez, Andrew Nayenko</li>
            <li>The <strong>star</strong> icon is licensed under the CCO license. The icon was retrieved from <a href="https://www.svgrepo.com/svg/13695/star">SVG repo</a></li>
            <li>The <strong>fork</strong> icon is licensed under the MIT license. The icon was retrieved from <a href="https://css.gg">css.gg</a> via <a href="https://www.svgrepo.com/svg/315651/git-fork">SVG repo</a></li>
            <li>The <strong>eye</strong> icon is licensed under the CCO license. The icon was retrieved from <a href="https://www.svgrepo.com/svg/42002/eye">SVG repo</a></li>
        </ul>
        <br>
        <p id="page-footer-text">Last update of this <span id="page-footer-hosting-name">GitLab Pages</span> page: <span id="page-timestamp-last-update" data-timestamp="""+'"'+str(datetime.now(tzutc()).strftime("%Y-%m-%dT%H:%M:000"))+'"'+""">"""+str(datetime.now(tzutc()).strftime("%Y-%m-%d at %H:%M (%Z)"))+"""</span>. The source code is located <a id="page-footer-source-code-link" href="https://gitlab.com/tinyweatherforecastgermanygroup/index">here</a>. Created by Jean-Luc Tibaux (<a href="https://gitlab.com/eUgEntOptIc44">@eUgEntOptIc44</a>). Licensed under <a id="page-footer-repo-license-link" href="https://gitlab.com/tinyweatherforecastgermanygroup/index/-/raw/main/LICENSE">GPLv3</a>. No Warranty or Guarantee of any kind provided. Use at your own risk only.</p>
    </div>
"""

htmlResultStr += "\n\t</div>\n</article>"

jsAssetUrls = list(assetsEnv['js_all'].urls())

for jsAssetUrlTemp in jsAssetUrls:
    htmlResultStr += '<script defer src="' + str(jsAssetUrlTemp) + '"></script>'

htmlResultStr += "\n</body>\n</html>"

indexHtmlFile = str(Path(workingDir / "index.html").absolute())
try:
    with open(indexHtmlFile, "w+", encoding="utf-8") as fh:
        fh.write(htmlmin.minify(htmlResultStr, remove_empty_space=True))
except Exception as e:
    logging.error("minification of '"+indexHtmlFile+"' failed -> error: "+str(e))
    with open(indexHtmlFile, "w+", encoding="utf-8") as fh:
        fh.write(htmlResultStr)

#sys.exit(0) #activate for dev only!

try:
    logging.debug(requests.get("https://www.google.com/ping?sitemap=https://tinyweatherforecastgermanygroup.gitlab.io/index/sitemap.xml").text)
except Exception as e:
    logging.error("failed to ping 'https://www.google.com/ping?sitemap=https://tinyweatherforecastgermanygroup.gitlab.io/index/sitemap.xml' for sitemap update -> error: "+str(e))

try:
    logging.debug(requests.get("http://bing.com/webmaster/ping.aspx?sitemap=https://tinyweatherforecastgermanygroup.gitlab.io/index/sitemap.xml").text)
except Exception as e:
    logging.error("failed to ping 'http://bing.com/webmaster/ping.aspx?sitemap=https://tinyweatherforecastgermanygroup.gitlab.io/index/sitemap.xml' for sitemap update -> error: "+str(e))

try:
    logging.debug(requests.get("https://webmaster.yandex.ru/ping?sitemap=https://tinyweatherforecastgermanygroup.gitlab.io/index/sitemap.xml").text)
except Exception as e:
    logging.error("failed to ping 'https://webmaster.yandex.ru/ping?sitemap=https://tinyweatherforecastgermanygroup.gitlab.io/index/sitemap.xml' for sitemap update -> error: "+str(e))

print("done")
logging.debug("completed script execution")
