"""

@title TinyWeatherForecastGermany - watchers - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty or guarantee of any kind provided. Use at your own risk.
Not meant to be used in commercial or in general critical/productive environments at all.

-------------

### TODO

* highlight contributing watchers?

"""

import base64
from datetime import datetime, date, timedelta
from pathlib import Path
from pprint import pprint
import logging
import json
import sys

from dateutil.tz import tzutc # timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
import htmlmin

from bs4 import BeautifulSoup

import markdown
from markdown.extensions.toc import TocExtension

import requests
import regex

workingDir = Path("public")
workingDir.mkdir(parents=True, exist_ok=True) # create directory if not exists

try:
    logging.basicConfig(format=u'%(asctime)-s %(levelname)s [%(name)s]: %(message)s',
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(Path(workingDir / "debug.log").absolute()), encoding="utf-8"),
            logging.StreamHandler()
    ])
except Exception as e:
    logging.error("while logger init! -> error: "+str(e))

def stringToBase64(string, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64encode(string.encode(encoding))

def base64ToString(base64str, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64decode(base64str).decode(encoding)

def json_rest_req(url_temp):
    
    url_temp = str(url_temp).strip()
    
    headers = {
        "DNT":"1"
    }

    logging.debug("preparing json api request for '"+url_temp+"' ")

    try:
        if len(url_temp) > 0:
            req_temp = requests.get(url_temp, headers=headers)

            if req_temp.status_code == 200:
                json_temp = json.loads(req_temp.text)

                if len(str(json_temp)) > 4:

                    if json_temp == None:
                        logging.error("content of parsed json api request anwser for '"+url_temp+"' is invalid! -> content: "+str(json_temp))
                        return []

                    return json_temp
                else:
                    logging.error("length of parsed json api request anwser for '"+url_temp+"' is invalid! ")
                    return []
    except Exception as e:
        logging.error("json api request for '"+url_temp+"' failed! -> error: "+str(e))
        return []

watchers_md = '\n# Watchers \n'
watchers_list = []

# ----------------

try:
    codeberg_watchers_json = json_rest_req('https://codeberg.org/api/v1/repos/starfish/TinyWeatherForecastGermany/subscribers')
    #pprint(codeberg_watchers_json)
except Exception as e:
    codeberg_watchers_json = []
    logging.error(f"while trying to fetch codeberg watchers via api -> {e}")

try:
    if len(codeberg_watchers_json) > 0:
        watchers_md += "\n## Codeberg \n"
        for codeberg_watcher in codeberg_watchers_json:
            try:
                watchers_md += f"\n* {codeberg_watcher['full_name']} [@{codeberg_watcher['login']}](https://codeberg.org/{codeberg_watcher['login']}/)"

                watchergazer_dict = {}
                if 'login' in codeberg_watcher:
                    watchergazer_dict['login'] = str(codeberg_watcher['login'])
                if 'full_name' in codeberg_watcher:
                    watchergazer_dict['full_name'] = str(codeberg_watcher['full_name'])
                
                if watchergazer_dict != {}:
                    watchergazer_dict['html_url'] = f"https://codeberg.org/{codeberg_watcher['login']}/"
                    watchers_list.append(watchergazer_dict)
            except Exception as e:
                logging.error(f"while trying to process codeberg watcher received from api -> {e}")
                logging.error(f"{codeberg_watcher}")
except Exception as e:
    logging.error(f"while trying to process codeberg watchers received from api -> {e}")

# ----------------

try:
    github_watchers_json = json_rest_req('https://api.github.com/repos/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany/watchers')
    #pprint(github_watchers_json)
except Exception as e:
    github_watchers_json = []
    logging.error(f"while trying to fetch github watchers via api -> {e}")

try:
    if len(github_watchers_json) > 0:
        watchers_md += "\n\n## GitHub \n"
        for github_watcher in github_watchers_json:
            try:
                watchers_md += f"\n* [@{github_watcher['login']}]({github_watcher['html_url']})"

                watchergazer_dict = {}
                if 'login' in github_watcher:
                    watchergazer_dict['login'] = str(github_watcher['login'])
                if 'html_url' in github_watcher:
                    watchergazer_dict['html_url'] = str(github_watcher['html_url'])
                
                if watchergazer_dict != {}:
                    watchergazer_dict['full_name'] = ''
                    watchers_list.append(watchergazer_dict)
            except Exception as e:
                logging.error(f"while trying to process github watcher received from api -> {e}")
                logging.error(f"{github_watcher}")
except Exception as e:
    logging.error(f"while trying to process github watchers received from api -> {e}")

# ----------------

try:
    gitea_watchers_json = json_rest_req('https://gitea.com/api/v1/repos/tinyweatherforecastgermanygroup/TinyWeatherForecastGermanyMirror/subscribers')
    #pprint(gitea_watchers_json)
except Exception as e:
    gitea_watchers_json = []
    logging.error(f"while trying to fetch gitea watchers via api -> {e}")

try:
    if len(gitea_watchers_json) > 0:
        watchers_md += "\n\n## Gitea \n"
        for gitea_watcher in gitea_watchers_json:
            try:
                watchers_md += f"\n* {gitea_watcher['full_name']} [@{gitea_watcher['login']}](https://gitea.com/{gitea_watcher['login']}/)"

                watchergazer_dict = {}
                if 'full_name' in gitea_watcher:
                    watchergazer_dict['full_name'] = str(gitea_watcher['full_name'])
                if 'login' in gitea_watcher:
                    watchergazer_dict['login'] = str(gitea_watcher['login'])
                
                if watchergazer_dict != {}:
                    watchergazer_dict['html_url'] = f"https://gitea.com/{gitea_watcher['login']}/"
                    watchers_list.append(watchergazer_dict)
            except Exception as e:
                logging.error(f"while trying to process gitea watcher received from api -> {e}")
                logging.error(f"{gitea_watcher}")
except Exception as e:
    logging.error(f"while trying to process gitea watchers received from api -> {e}")


with open(str(Path(workingDir / "watchers.md").absolute()), "w", encoding="utf-8") as fh:
    fh.write(str(watchers_md))
with open(str(Path(workingDir / "watchers.json").absolute()), "w", encoding="utf-8") as fh:
    fh.write(str(json.dumps(watchers_list, indent=4)))

# --------------------------------------------- #

indexFile = Path(workingDir / "index.html") # we're using index.html as template
indexFileContents = ""
if indexFile.exists():
    if indexFile.stat().st_size > 0:
        logging.debug("found file 'index.html' -> size: "+str(indexFile.stat().st_size))
        with open(str(indexFile.absolute()), "r", encoding="utf-8") as fh:
            indexFileContents = str(fh.read())
    else:
        logging.error("found file 'index.html' but size '"+str(indexFile.stat().st_size)+"' is invalid! -> this might occur due to missing permissions! ")
else:
    logging.warning("could not find the file 'index.html' -> using minimal hardcoded template instead")

watchers_md = "\n\n[TOC]\n\n" + watchers_md # add placeholder for table of contents 
watchers_html = markdown.markdown(watchers_md, extensions=['extra', 'sane_lists', TocExtension(baselevel=2, title='Table of contents', anchorlink=True, toc_depth="3-5")]) # 'nl2br',

watchers_html_soup = BeautifulSoup(watchers_html, features='html.parser') # parse html to insert elements into 'index.html'
#print(watchers_html_soup)

if len(str(indexFileContents)) > 0 and "html" in str(indexFileContents):
    indexFileSoup = BeautifulSoup(indexFileContents, features='html.parser') # parse html to modify elements

    indexFileSoup.title.string = "watchers | open source android app using open weather data from Deutscher Wetterdienst (DWD)"

    if len(indexFileSoup.select("#repo-latest-release-container")) > 0:
        indexFileSoup.select("#repo-latest-release-container")[0].decompose()

    if len(indexFileSoup.select("#readme-content-container")) > 0:
        indexFileSoup.select("#readme-content-container")[0].decompose()

    if len(indexFileSoup.select("#repo-metadata-container")) > 0:
        indexFileSoup.select("#repo-metadata-container")[0].insert_after(watchers_html_soup)

    indexFileSoup.select("#page-timestamp-last-update")[0].string = str(datetime.now(tzutc()).strftime("%Y-%m-%d at %H:%M (%Z)"))
    indexFileSoup.select("#page-timestamp-last-update")[0]["data-timestamp"] = str(datetime.now(tzutc()).strftime("%Y-%m-%dT%H:%M:000"))

    watchers_html = str(indexFileSoup)
else:
    watchers_html = "<html><head></head><body>"+watchers_html+"<body></html>"

watchers_html_file = Path(workingDir / "watchers.html").absolute()

try:
    with open(str(watchers_html_file), "w+", encoding="utf-8") as fh:
        fh.write(htmlmin.minify(watchers_html, remove_empty_space=True))
except Exception as e:
    logging.error("minification of '"+str(watchers_html_file)+"' failed -> error: "+str(e))
    with open(watchers_html_file, "w+", encoding="utf-8") as fh:
        fh.write(watchers_html)

print("done")
