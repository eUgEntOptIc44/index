"""

@title TinyWeatherForecastGermany - stars - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty or guarantee of any kind provided. Use at your own risk.
Not meant to be used in commercial or in general critical/productive environments at all.

-------------

### TODO

* highlight contributing stargazers?

"""

import base64
from datetime import datetime, date, timedelta
from pathlib import Path
from pprint import pprint
import logging
import json
import sys

from dateutil.tz import tzutc # timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
import htmlmin

from bs4 import BeautifulSoup

import markdown
from markdown.extensions.toc import TocExtension

import requests
import regex

workingDir = Path("public")
workingDir.mkdir(parents=True, exist_ok=True) # create directory if not exists

try:
    logging.basicConfig(format=u'%(asctime)-s %(levelname)s [%(name)s]: %(message)s',
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(Path(workingDir / "debug.log").absolute()), encoding="utf-8"),
            logging.StreamHandler()
    ])
except Exception as e:
    logging.error("while logger init! -> error: "+str(e))

def stringToBase64(string, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64encode(string.encode(encoding))

def base64ToString(base64str, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64decode(base64str).decode(encoding)

def json_rest_req(url_temp):
    
    url_temp = str(url_temp).strip()
    
    headers = {
        "DNT":"1"
    }

    logging.debug("preparing json api request for '"+url_temp+"' ")

    try:
        if len(url_temp) > 0:
            req_temp = requests.get(url_temp, headers=headers)

            if req_temp.status_code == 200:
                json_temp = json.loads(req_temp.text)

                if len(str(json_temp)) > 4:

                    if json_temp == None:
                        logging.error("content of parsed json api request anwser for '"+url_temp+"' is invalid! -> content: "+str(json_temp))
                        return []

                    return json_temp
                else:
                    logging.error("length of parsed json api request anwser for '"+url_temp+"' is invalid! ")
                    return []
    except Exception as e:
        logging.error("json api request for '"+url_temp+"' failed! -> error: "+str(e))
        return []

stargazers_md = '\n# Stargazers \n'
stargazers_list = []

# ----------------

try:
    codeberg_stars_json = json_rest_req('https://codeberg.org/api/v1/repos/Starfish/TinyWeatherForecastGermany/stargazers')
    #pprint(codeberg_stars_json)
except Exception as e:
    codeberg_stars_json = []
    logging.error(f"while trying to fetch codeberg stars via api -> {e}")

try:
    if len(codeberg_stars_json) > 0:
        stargazers_md += "\n## Codeberg \n"
        for codeberg_star in codeberg_stars_json:
            try:
                stargazers_md += f"\n* {codeberg_star['full_name']} [@{codeberg_star['login']}](https://codeberg.org/{codeberg_star['login']}/)"

                stargazer_dict = {}
                if 'login' in codeberg_star:
                    stargazer_dict['login'] = str(codeberg_star['login'])
                if 'full_name' in codeberg_star:
                    stargazer_dict['full_name'] = str(codeberg_star['full_name'])
                
                if stargazer_dict != {}:
                    stargazer_dict['html_url'] = f"https://codeberg.org/{codeberg_star['login']}/"
                    stargazers_list.append(stargazer_dict)
            except Exception as e:
                logging.error(f"while trying to process codeberg star received from api -> {e}")
                logging.error(f"{codeberg_star}")
except Exception as e:
    logging.error(f"while trying to process codeberg stars received from api -> {e}")

# ----------------

try:
    github_stars_json = json_rest_req('https://api.github.com/repos/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany/stargazers')
    #pprint(github_stars_json)
except Exception as e:
    github_stars_json = []
    logging.error(f"while trying to fetch github stars via api -> {e}")

try:
    if len(github_stars_json) > 0:
        stargazers_md += "\n\n## GitHub \n"
        for github_star in github_stars_json:
            try:
                stargazers_md += f"\n* [@{github_star['login']}]({github_star['html_url']})"

                stargazer_dict = {}
                if 'login' in github_star:
                    stargazer_dict['login'] = str(github_star['login'])
                if 'html_url' in github_star:
                    stargazer_dict['html_url'] = str(github_star['html_url'])
                
                if stargazer_dict != {}:
                    stargazer_dict['full_name'] = ''
                    stargazers_list.append(stargazer_dict)
            except Exception as e:
                logging.error(f"while trying to process github star received from api -> {e}")
                logging.error(f"{github_star}")
except Exception as e:
    logging.error(f"while trying to process github stars received from api -> {e}")

# ----------------

try:
    gitlab_stars_json = json_rest_req('https://gitlab.com/api/v4/projects/tinyweatherforecastgermanygroup%2FTinyWeatherForecastGermany/starrers')
    #pprint(gitlab_stars_json)
except Exception as e:
    gitlab_stars_json = []
    logging.error(f"while trying to fetch gitlab stars via api -> {e}")

try:
    if len(gitlab_stars_json) > 0:
        stargazers_md += "\n\n## GitLab \n"
        for gitlab_star in gitlab_stars_json:
            try:
                stargazers_md += f"\n* {gitlab_star['user']['name']} [@{gitlab_star['user']['username']}]({gitlab_star['user']['web_url']})"
                
                stargazer_dict = {}
                if 'username' in gitlab_star['user']:
                    stargazer_dict['full_name'] = str(gitlab_star['user']['username'])
                if 'name' in gitlab_star['user']:
                    stargazer_dict['login'] = str(gitlab_star['user']['name'])
                if 'web_url' in gitlab_star['user']:
                    stargazer_dict['html_url'] = str(gitlab_star['user']['web_url'])
                
                if stargazer_dict != {}:
                    stargazers_list.append(stargazer_dict)
            except Exception as e:
                logging.error(f"while trying to process gitlab star received from api -> {e}")
                logging.error(f"{gitlab_star}")
except Exception as e:
    logging.error(f"while trying to process gitlab stars received from api -> {e}")

# ----------------

try:
    framagit_stars_json = json_rest_req('https://framagit.org/api/v4/projects/tinyweatherforecastgermanygroup%2Ftinyweatherforecastgermanymirror/starrers')
    #pprint(framagit_stars_json)
except Exception as e:
    framagit_stars_json = []
    logging.error(f"while trying to fetch framagit stars via api -> {e}")

try:
    if len(framagit_stars_json) > 0:
        stargazers_md += "\n\n## Framagit \n"
        for framagit_star in framagit_stars_json:
            try:
                stargazers_md += f"\n* {framagit_star['user']['name']} [@{framagit_star['user']['username']}]({framagit_star['user']['web_url']})"
                
                stargazer_dict = {}
                if 'username' in framagit_star['user']:
                    stargazer_dict['full_name'] = str(framagit_star['user']['username'])
                if 'name' in framagit_star['user']:
                    stargazer_dict['login'] = str(framagit_star['user']['name'])
                if 'web_url' in framagit_star['user']:
                    stargazer_dict['html_url'] = str(framagit_star['user']['web_url'])
                
                if stargazer_dict != {}:
                    stargazers_list.append(stargazer_dict)
            except Exception as e:
                logging.error(f"while trying to process framagit star received from api -> {e}")
                logging.error(f"{framagit_star}")
except Exception as e:
    logging.error(f"while trying to process framagit stars received from api -> {e}")

# ----------------

try:
    gitea_stars_json = json_rest_req('https://gitea.com/api/v1/repos/tinyweatherforecastgermanygroup/TinyWeatherForecastGermanyMirror/stargazers')
    #pprint(gitea_stars_json)
except Exception as e:
    gitea_stars_json = []
    logging.error(f"while trying to fetch gitea stars via api -> {e}")

try:
    if len(gitea_stars_json) > 0:
        stargazers_md += "\n\n## Gitea \n"
        for gitea_star in gitea_stars_json:
            try:
                stargazers_md += f"\n* {gitea_star['full_name']} [@{gitea_star['login']}](https://gitea.com/{gitea_star['login']}/)"

                stargazer_dict = {}
                if 'full_name' in gitea_star:
                    stargazer_dict['full_name'] = str(gitea_star['full_name'])
                if 'login' in gitea_star:
                    stargazer_dict['login'] = str(gitea_star['login'])
                
                if stargazer_dict != {}:
                    stargazer_dict['html_url'] = f"https://gitea.com/{gitea_star['login']}/"
                    stargazers_list.append(stargazer_dict)
            except Exception as e:
                logging.error(f"while trying to process gitea star received from api -> {e}")
                logging.error(f"{gitea_star}")
except Exception as e:
    logging.error(f"while trying to process gitea stars received from api -> {e}")


with open(str(Path(workingDir / "stargazers.md").absolute()), "w", encoding="utf-8") as fh:
    fh.write(str(stargazers_md))
with open(str(Path(workingDir / "stargazers.json").absolute()), "w", encoding="utf-8") as fh:
    fh.write(str(json.dumps(stargazers_list, indent=4)))

# --------------------------------------------- #

indexFile = Path(workingDir / "index.html") # we're using index.html as template
indexFileContents = ""
if indexFile.exists():
    if indexFile.stat().st_size > 0:
        logging.debug("found file 'index.html' -> size: "+str(indexFile.stat().st_size))
        with open(str(indexFile.absolute()), "r", encoding="utf-8") as fh:
            indexFileContents = str(fh.read())
    else:
        logging.error("found file 'index.html' but size '"+str(indexFile.stat().st_size)+"' is invalid! -> this might occur due to missing permissions! ")
else:
    logging.warning("could not find the file 'index.html' -> using minimal hardcoded template instead")

stargazers_md = "\n\n[TOC]\n\n" + stargazers_md # add placeholder for table of contents 
stargazers_html = markdown.markdown(stargazers_md, extensions=['extra', 'sane_lists', TocExtension(baselevel=2, title='Table of contents', anchorlink=True, toc_depth="3-5")]) # 'nl2br',

stargazers_html_soup = BeautifulSoup(stargazers_html, features='html.parser') # parse html to insert elements into 'index.html'
#print(stargazers_html_soup)

if len(str(indexFileContents)) > 0 and "html" in str(indexFileContents):
    indexFileSoup = BeautifulSoup(indexFileContents, features='html.parser') # parse html to modify elements

    indexFileSoup.title.string = "stargazers | open source android app using open weather data from Deutscher Wetterdienst (DWD)"

    if len(indexFileSoup.select("#repo-latest-release-container")) > 0:
        indexFileSoup.select("#repo-latest-release-container")[0].decompose()

    if len(indexFileSoup.select("#readme-content-container")) > 0:
        indexFileSoup.select("#readme-content-container")[0].decompose()

    if len(indexFileSoup.select("#repo-metadata-container")) > 0:
        indexFileSoup.select("#repo-metadata-container")[0].insert_after(stargazers_html_soup)

    indexFileSoup.select("#page-timestamp-last-update")[0].string = str(datetime.now(tzutc()).strftime("%Y-%m-%d at %H:%M (%Z)"))
    indexFileSoup.select("#page-timestamp-last-update")[0]["data-timestamp"] = str(datetime.now(tzutc()).strftime("%Y-%m-%dT%H:%M:000"))

    stargazers_html = str(indexFileSoup)
else:
    stargazers_html = "<html><head></head><body>"+stargazers_html+"<body></html>"

stargazers_html_file = Path(workingDir / "stargazers.html").absolute()

try:
    with open(str(stargazers_html_file), "w+", encoding="utf-8") as fh:
        fh.write(htmlmin.minify(stargazers_html, remove_empty_space=True))
except Exception as e:
    logging.error("minification of '"+str(stargazers_html_file)+"' failed -> error: "+str(e))
    with open(stargazers_html_file, "w+", encoding="utf-8") as fh:
        fh.write(stargazers_html)

print("done")
