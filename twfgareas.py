"""

@title TinyWeatherForecastGermany - areas.txt contents - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty or guarantee of any kind provided. Use at your own risk.
Not meant to be used in commercial or in general critical/productive environments at all.

"""

import base64
from datetime import datetime, date, timedelta
from pathlib import Path
from pprint import pprint
import logging
import json

from dateutil.tz import tzutc # timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
import htmlmin

from bs4 import BeautifulSoup

import requests
import regex

workingDir = Path("public")
workingDir.mkdir(parents=True, exist_ok=True) # create directory if not exists

try:
    logging.basicConfig(format=u'%(asctime)-s %(levelname)s [%(name)s]: %(message)s',
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(Path(workingDir / "debug.log").absolute()), encoding="utf-8"),
            logging.StreamHandler()
    ])
except Exception as e:
    logging.error("while logger init! -> error: "+str(e))

ApiBaseUrl = "https://codeberg.org/api/v1"

ApiRepoUrl = ApiBaseUrl + "/repos/Starfish/TinyWeatherForecastGermany"

ApiContentsUrl = ApiRepoUrl + "/contents"

def stringToBase64(string, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64encode(string.encode(encoding))

def base64ToString(base64str, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64decode(base64str).decode(encoding)

def jsonRestReq(urlTemp):
    
    urlTemp = str(urlTemp).strip()
    
    headers = {}

    logging.debug("preparing json api request for '"+urlTemp+"' ")

    try:
        if len(urlTemp) > 0:
            reqTemp = requests.get(urlTemp, headers=headers)

            if reqTemp.status_code == 200:
                jsonTemp = json.loads(reqTemp.text)

                if len(str(jsonTemp)) > 4:

                    if jsonTemp == None:
                        logging.error("content of parsed json api request anwser for '"+urlTemp+"' is invalid! -> content: "+str(jsonTemp))
                        return []

                    return jsonTemp
                else:
                    logging.error("length of parsed json api request anwser for '"+urlTemp+"' is invalid! ")
                    return []
    except Exception as e:
        logging.error("json api request for '"+urlTemp+"' failed! -> error: "+str(e))
        return []

areasFileJson = jsonRestReq(ApiContentsUrl + "/app/src/main/res/raw/areas.txt") # data source: https://codeberg.org/Starfish/TinyWeatherForecastGermany/src/branch/master/app/src/main/res/raw/areas.txt
#print(areasFileJson)

with open(str(Path(workingDir / "twfg-areas-temp.json").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(json.dumps(areasFileJson, indent=4)))
#with open(str(Path(workingDir / "twfg-areas-temp.json").absolute()), "r", encoding="utf-8") as fh:
#    areasFileJson = json.loads(str(fh.read()))

areasFileContent = str(areasFileJson["content"])
areasFileEncoding = str(areasFileJson["encoding"]).strip()

if areasFileEncoding == "base64":
    areasFileContent = str(base64ToString(areasFileJson["content"]))
else:
    logging.warning("detected unexpected file encoding '"+areasFileEncoding+"' of 'areas.txt' ("+str(areasFileJson["url"])+") ")

with open(str(Path(workingDir / "areas.txt").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(areasFileContent)
#with open(str(Path(workingDir / "areas.txt").absolute()), "r", encoding="utf-8") as fh:
#    areasFileContent = str(fh.read())

areasFileLines = list(areasFileContent.split("\n"))

"""
data format -> split by '@':

    example: 209902000@206@2@"Rothsee"@49.211563,11.18254 49.226807,11.194911 49.23832,11.208389 49.22707,11.188899 49.225174,11.176717 49.211563,11.18254

    0 -> warncellID -> 209902000
    1 -> warncenter -> 206
    2 -> type -> 2
    3 -> name -> Rothsee # quotes have to be removed
    4 -> polygonString
            49.211563
            11.18254 49.226807
            11.194911 49.23832
            11.208389 49.22707
            11.188899 49.225174
            11.176717 49.211563
            11.18254
"""

resultListParsedAreas = []

for lineIndex in range(len(areasFileLines)):
    try:
        if lineIndex > 0: # line 0 contains the metadata e.g. Geodata version: 2, 12.06.2021
            lineTemp = str(areasFileLines[lineIndex]).strip()
            if len(lineTemp) < 5:
                logging.warning("skipping line "+str(lineIndex + 1)+" of 'areas.txt' -> length < 5 ")
                if len(lineTemp) > 0:
                    logging.debug(lineTemp)
                continue
            if "@" in lineTemp:
                lineTempParts = lineTemp.split("@")
                
                if len(lineTempParts) == 5:
                    resultListParsedAreas.append({
                        "warncellID":int(lineTempParts[0]),
                        "warncenter":int(lineTempParts[1]),
                        "type":int(lineTempParts[2]),
                        "name":str(lineTempParts[3]).strip().strip('"').strip("'"),
                        "polygonString":str(lineTempParts[4]).strip()
                    })
            else:
                logging.error("while parsing line "+str(lineIndex + 1)+" of 'areas.txt' -> error: failed to find '@' ")
                logging.debug(lineTemp)
    except Exception as e:
        logging.error("while parsing line "+str(lineIndex + 1)+" of 'areas.txt' -> error: "+str(e))
        logging.error(str(areasFileLines[lineIndex]))

lenResultListParsedAreas = len(resultListParsedAreas)

if lenResultListParsedAreas == 0:
    logging.error("while parsing 'areas.txt' -> error: no parsed data in 'resultListParsedAreas' -> invalid length '"+str(lenResultListParsedAreas)+"' !")
else:
    logging.debug("successfully parsed "+str(lenResultListParsedAreas)+" areas from 'areas.txt'")

with open(str(Path(workingDir / "twfg-areas.json").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(json.dumps(resultListParsedAreas, indent=4))
#with open(str(Path(workingDir / "twfg-areas.json").absolute()), "r", encoding="utf-8") as fh:
#    lenResultListParsedAreas = json.loads(str(fh.read()))

indexFile = Path(workingDir / "index.html") # we're using index.html as template
indexFileContents = ""
if indexFile.exists():
    if indexFile.stat().st_size > 0:
        logging.debug("found file 'index.html' -> size: "+str(indexFile.stat().st_size))
        with open(str(indexFile.absolute()), "r", encoding="utf-8") as fh:
            indexFileContents = str(fh.read())
    else:
        logging.error("found file 'index.html' but size '"+str(indexFile.stat().st_size)+"' is invalid! -> this might occur due to missing permissions! ")
else:
    logging.warning("could not find the file 'index.html' -> using minimal hardcoded template instead")

try:
    def getWarnCellID(itemTemp):
        return itemTemp["warncellID"]
    resultListParsedAreas = sorted(resultListParsedAreas, key=getWarnCellID)
except Exception as e:
    logging.error("sorting of 'resultListParsedAreas' failed! -> error: "+str(e))

areasHTMLstr = '<div id="areas-table-container"><table id="areas-table">'

areasHTMLstr += '<thead><tr id="areas-table-headings">'
for areasHeadTemp in resultListParsedAreas[0].keys():
    if str(areasHeadTemp).strip().lower() != "polygonstring":
        slugTemp = str(regex.sub(r"[^A-z0-9]+","",str(areasHeadTemp).strip()))
        areasHTMLstr += '<th class="areas-table-heading" data-sort="'+str(areasHeadTemp).strip()+'" data-sort-order="desc" id="areas-table-heading-'+slugTemp+'">'+str(areasHeadTemp).strip()+'</th>'
areasHTMLstr += '</tr></thead><tbody class="list">'

for areaIndexTemp in range(len(resultListParsedAreas)):
    areaTemp = resultListParsedAreas[areaIndexTemp]
    areasHTMLstr += '<tr class="areas-table-row" data-id="'+str(areaIndexTemp)+'" id="areas-table-row-'+str(areaIndexTemp+1)+'"><td class="warncellID">'+str(areaTemp["warncellID"])+'</td><td class="warncenter">'+str(areaTemp["warncenter"])+'</td><td class="type">'+str(areaTemp["type"])+'</td><td class="name">'+str(areaTemp["name"])+'</td></tr>'

areasHTMLstr += '</tbody></table><ul id="areas-table-pagination" class="pagination"></ul></div>'

areasHTMLsoup = BeautifulSoup(areasHTMLstr, features='html.parser') # parse html to insert elements into 'index.html'
#print(areasHTMLsoup)

if len(str(indexFileContents)) > 0 and "html" in str(indexFileContents):
    indexFileSoup = BeautifulSoup(indexFileContents, features='html.parser') # parse html to modify elements

    indexFileSoup.title.string = "areas.txt | open source android app using open weather data from Deutscher Wetterdienst (DWD)"

    if len(indexFileSoup.select("#repo-latest-release-container")) > 0:
        indexFileSoup.select("#repo-latest-release-container")[0].decompose()

    if len(indexFileSoup.select("#readme-content-container")) > 0:
        indexFileSoup.select("#readme-content-container")[0].decompose()

    if len(indexFileSoup.select("#repo-metadata-container")) > 0:
        indexFileSoup.select("#repo-metadata-container")[0].insert_after(areasHTMLsoup)

    indexFileSoup.select("#page-timestamp-last-update")[0].string = str(datetime.now(tzutc()).strftime("%Y-%m-%d at %H:%M (%Z)"))
    indexFileSoup.select("#page-timestamp-last-update")[0]["data-timestamp"] = str(datetime.now(tzutc()).strftime("%Y-%m-%dT%H:%M:000"))

    areasHTMLstr = str(indexFileSoup)
else:
    areasHTMLstr = "<html><head></head><body>"+areasHTMLstr+"<body></html>"

areasHtmlFile = Path(workingDir / "areas.html").absolute()

try:
    with open(areasHtmlFile, "w+", encoding="utf-8") as fh:
        fh.write(htmlmin.minify(areasHTMLstr, remove_empty_space=True))
except Exception as e:
    logging.error("minification of '"+areasHtmlFile+"' failed -> error: "+str(e))
    with open(areasHtmlFile, "w+", encoding="utf-8") as fh:
        fh.write(areasHTMLstr)

print("done")
