"""

@title TinyWeatherForecastGermany - stations4.txt contents - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty or guarantee of any kind provided. Use at your own risk.
Not meant to be used in commercial or in general critical/productive environments at all.

"""

import base64
from datetime import datetime, date, timedelta
from pathlib import Path
from pprint import pprint
import logging
import json
import sys

from dateutil.tz import tzutc # timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
import htmlmin

from bs4 import BeautifulSoup

import requests
import regex

workingDir = Path("public")
workingDir.mkdir(parents=True, exist_ok=True) # create directory if not exists

try:
    logging.basicConfig(format=u'%(asctime)-s %(levelname)s [%(name)s]: %(message)s',
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(Path(workingDir / "debug.log").absolute()), encoding="utf-8"),
            logging.StreamHandler()
    ])
except Exception as e:
    logging.error("while logger init! -> error: "+str(e))

ApiBaseUrl = "https://codeberg.org/api/v1"

ApiRepoUrl = ApiBaseUrl + "/repos/Starfish/TinyWeatherForecastGermany"

ApiContentsUrl = ApiRepoUrl + "/contents"

def stringToBase64(string, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64encode(string.encode(encoding))

def base64ToString(base64str, encoding='utf-8'): # based on: https://stackoverflow.com/a/13267587 - License: CC BY-SA 3.0 - user: Sheena
    return base64.b64decode(base64str).decode(encoding)

def jsonRestReq(urlTemp):
    
    urlTemp = str(urlTemp).strip()
    
    headers = {}

    logging.debug("preparing json api request for '"+urlTemp+"' ")

    try:
        if len(urlTemp) > 0:
            reqTemp = requests.get(urlTemp, headers=headers)

            if reqTemp.status_code == 200:
                jsonTemp = json.loads(reqTemp.text)

                if len(str(jsonTemp)) > 4:

                    if jsonTemp == None:
                        logging.error("content of parsed json api request anwser for '"+urlTemp+"' is invalid! -> content: "+str(jsonTemp))
                        return []

                    return jsonTemp
                else:
                    logging.error("length of parsed json api request anwser for '"+urlTemp+"' is invalid! ")
                    return []
    except Exception as e:
        logging.error("json api request for '"+urlTemp+"' failed! -> error: "+str(e))
        return []

stationsFileJson = jsonRestReq(ApiContentsUrl + "/app/src/main/res/raw/stations4.txt") # data source: https://codeberg.org/Starfish/TinyWeatherForecastGermany/src/branch/master/app/src/main/res/raw/stations4.txt
#print(stationsFileJson)

with open(str(Path(workingDir / "twfg-stations-temp.json").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(json.dumps(stationsFileJson, indent=4)))
#with open(str(Path(workingDir / "twfg-stations-temp.json").absolute()), "r", encoding="utf-8") as fh:
#    stationsFileJson = json.loads(str(fh.read()))

if stationsFileJson == None:
    logging.error(f"content of 'stationsFileJson' is None -> FATAL ERROR ")
    sys.exit(1)

stationsFileContent = str(stationsFileJson["content"])
stationsFileEncoding = str(stationsFileJson["encoding"]).strip()

if stationsFileEncoding == "base64":
    stationsFileContent = str(base64ToString(stationsFileJson["content"]))
else:
    logging.warning("detected unexpected file encoding '"+stationsFileEncoding+"' of 'stations4.txt' ("+str(stationsFileJson["url"])+") ")

with open(str(Path(workingDir / "stations4.txt").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(stationsFileContent)
#with open(str(Path(workingDir / "stations4.txt").absolute()), "r", encoding="utf-8") as fh:
#    stationsFileContent = str(fh.read())

stationsFileLines = list(stationsFileContent.split("|"))

"""
data format -> split by ';':

    example: 
      0 -> TROMSOE -> name
      1 -> 01025 -> id
      2 -> 18.92,69.68,10.0 -> needs to be split by ',' 
"""

resultListParsedStations = []

for lineIndex in range(len(stationsFileLines)):
    try:
        if lineIndex > -1:
            lineTemp = str(stationsFileLines[lineIndex]).strip()
            if len(lineTemp) < 5:
                logging.warning("skipping line "+str(lineIndex + 1)+" of 'stations4.txt' -> length < 5 ")
                if len(lineTemp) > 0:
                    logging.debug(lineTemp)
                continue
            if ";" in lineTemp:
                lineTempParts = lineTemp.split(";")
                #pprint(lineTempParts)
                
                if len(lineTempParts) == 3:
                    lineTempParts2 = str(lineTempParts[2]).split(",")

                    resultListParsedStations.append({
                        "name":str(lineTempParts[0]),
                        "id":str(lineTempParts[1]),
                        "latitude":float(lineTempParts2[1]),
                        "longtitude":float(lineTempParts2[0]),
                        "altitude":float(lineTempParts2[2])
                    })
            else:
                logging.error("while parsing line "+str(lineIndex + 1)+" of 'stations4.txt' -> error: failed to find '@' ")
                logging.debug(lineTemp)
    except Exception as e:
        logging.error("while parsing line "+str(lineIndex + 1)+" of 'stations4.txt' -> error: "+str(e))
        logging.error(str(stationsFileLines[lineIndex]))

lenResultListParsedStations = len(resultListParsedStations)

if lenResultListParsedStations == 0:
    logging.error("while parsing 'stations4.txt' -> error: no parsed data in 'resultListParsedStations' -> invalid length '"+str(lenResultListParsedStations)+"' !")
    sys.exit(0)
else:
    logging.debug("successfully parsed "+str(lenResultListParsedStations)+" stations from 'stations4.txt'")

with open(str(Path(workingDir / "twfg-stations.json").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(json.dumps(resultListParsedStations, indent=4))
#with open(str(Path(workingDir / "twfg-stations.json").absolute()), "r", encoding="utf-8") as fh:
#    lenResultListParsedStations = json.loads(str(fh.read()))

indexFile = Path(workingDir / "index.html") # we're using index.html as template
indexFileContents = ""
if indexFile.exists():
    if indexFile.stat().st_size > 0:
        logging.debug("found file 'index.html' -> size: "+str(indexFile.stat().st_size))
        with open(str(indexFile.absolute()), "r", encoding="utf-8") as fh:
            indexFileContents = str(fh.read())
    else:
        logging.error("found file 'index.html' but size '"+str(indexFile.stat().st_size)+"' is invalid! -> this might occur due to missing permissions! ")
else:
    logging.warning("could not find the file 'index.html' -> using minimal hardcoded template instead")

try:
    def getID(itemTemp):
        return str(itemTemp["id"])
    resultListParsedStations = sorted(resultListParsedStations, key=getID)
except Exception as e:
    logging.error("sorting of 'resultListParsedStations' failed! -> error: "+str(e))

stationsHTMLstr = '<div id="stations-table-container"><table id="stations-table">'

stationsHTMLstr += '<thead><tr id="stations-table-headings">'
for stationsHeadTemp in resultListParsedStations[0].keys():
    if str(stationsHeadTemp).strip().lower() != "none":
        slugTemp = str(regex.sub(r"[^A-z0-9]+","",str(stationsHeadTemp).strip()))
        stationsHTMLstr += '<th class="stations-table-heading" data-sort="'+str(stationsHeadTemp).strip()+'" data-sort-order="desc" id="stations-table-heading-'+slugTemp+'">'+str(stationsHeadTemp).strip()+'</th>'
stationsHTMLstr += '</tr></thead><tbody class="list">'

for stationIndexTemp in range(len(resultListParsedStations)):
    stationTemp = resultListParsedStations[stationIndexTemp]
    stationsHTMLstr += '<tr class="stations-table-row" data-id="'+str(stationIndexTemp)+'" id="stations-table-row-'+str(stationIndexTemp+1)+'"><td class="name">'+str(stationTemp["name"])+'</td><td class="id">'+str(stationTemp["id"])+'</td><td class="latitude">'+str(stationTemp["latitude"])+'</td><td class="longtitude">'+str(stationTemp["longtitude"])+'</td><td class="altitude">'+str(stationTemp["altitude"])+'</td></tr>'

stationsHTMLstr += '</tbody></table><ul id="stations-table-pagination" class="pagination"></ul></div>'

stationsHTMLsoup = BeautifulSoup(stationsHTMLstr, features='html.parser') # parse html to insert elements into 'index.html'
#print(stationsHTMLsoup)

if len(str(indexFileContents)) > 0 and "html" in str(indexFileContents):
    indexFileSoup = BeautifulSoup(indexFileContents, features='html.parser') # parse html to modify elements

    indexFileSoup.title.string = "stations4.txt | open source android app using open weather data from Deutscher Wetterdienst (DWD)"

    if len(indexFileSoup.select("#repo-latest-release-container")) > 0:
        indexFileSoup.select("#repo-latest-release-container")[0].decompose()

    if len(indexFileSoup.select("#readme-content-container")) > 0:
        indexFileSoup.select("#readme-content-container")[0].decompose()

    if len(indexFileSoup.select("#repo-metadata-container")) > 0:
        indexFileSoup.select("#repo-metadata-container")[0].insert_after(stationsHTMLsoup)

    indexFileSoup.select("#page-timestamp-last-update")[0].string = str(datetime.now(tzutc()).strftime("%Y-%m-%d at %H:%M (%Z)"))
    indexFileSoup.select("#page-timestamp-last-update")[0]["data-timestamp"] = str(datetime.now(tzutc()).strftime("%Y-%m-%dT%H:%M:000"))

    stationsHTMLstr = str(indexFileSoup)
else:
    stationsHTMLstr = "<html><head></head><body>"+stationsHTMLstr+"<body></html>"

stationsHtmlFile = Path(workingDir / "stations.html").absolute()

try:
    with open(str(stationsHtmlFile), "w+", encoding="utf-8") as fh:
        fh.write(htmlmin.minify(stationsHTMLstr, remove_empty_space=True))
except Exception as e:
    logging.error("minification of '"+str(stationsHtmlFile)+"' failed -> error: "+str(e))
    with open(stationsHtmlFile, "w+", encoding="utf-8") as fh:
        fh.write(stationsHTMLstr)

print("done")
