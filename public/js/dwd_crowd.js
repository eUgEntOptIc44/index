function dwd_crowd_meldungen() {
    try {
        var folium_map = document.querySelector('.folium-map');
        if(folium_map != undefined){
            console.log("found folium map", folium_map);
            var dwdmeldungen = L.markerClusterGroup();
            const addMeldungMarker = async (meldung) => {
                //console.log(meldung);
                var marker = new L.Marker([meldung['lat'], meldung['lon']]);
                marker.addTo(dwdmeldungen);
    
                var image_str = '';
                if('imageMediumUrl' in meldung && meldung['imageMediumUrl'] != undefined){
                    image_str = '<img title="©DWD" src="'+meldung['imageMediumUrl']+'" style="max-width:98.9%;" loading="lazy">';
                }
                var timestamp_str = '';
                try {
                    const unixtimestamp = new Date(meldung['timestamp']);
                    timestamp_str =  '<br>' + unixtimestamp.toLocaleString("de-DE") + '<br>';
                }catch(error){
                    console.log("failed to parse timestamp for dwd crowd meldung -> error: "+error);
                }
                marker.bindPopup('<strong>'+meldung['place']+'</strong><br>'+meldung['category']+'<br>'+meldung['auspraegung']+'<br>'+meldung['likeCount']+' likes'+timestamp_str+image_str).openPopup();
            };
    
            fetch('https://s3.eu-central-1.amazonaws.com/app-prod-static.warnwetter.de/v16/crowd_meldungen_overview_v2.json', { 
                method: 'GET'
            })
            .then(function(response) { return response.json(); })
            .then(function(json) {
                console.log(json);
                console.log('received '+json['meldungen'].length+' crowd meldungen by DWD via AWS');
                json['meldungen'].forEach(async (element) => {
                    try {
                        await addMeldungMarker(element);
                    }catch(error){
                        console.log("failed to process element of json['meldungen'] -> error: "+error);
                    }
                });
                twfg_folium_map.addLayer(dwdmeldungen);
            });
        }
    } catch (error) {
        console.log("ERROR: failed to execute dwd_crowd_meldungen -> error: " + error);
    }
}

document.addEventListener('DOMContentLoaded', function () {
    console.log("DEBUG: received event 'DOMContentLoaded' ");
    try {
        var map_layer_control = document.querySelector('.leaflet-control-layers-overlays');
        if(map_layer_control != undefined){
            console.log("DEBUG: creating the 'DWD crowd' label ... ");
            var label_container = document.createElement('label');
            var label_div = document.createElement('div');
            label_div.innerHTML = 'DWD crowd';
            label_div.setAttribute("title","External Data: fetched from AWS -> submitted by WarnWetter users.");
            label_container.appendChild(label_div);
            map_layer_control.appendChild(label_container);
            label_container.addEventListener('click', function (event) {
                event.preventDefault();
                console.log(event);
                console.log("DEBUG: received click event for 'DWD crowd' ");
                dwd_crowd_meldungen();
            });
        }   
    } catch (error) {
        console.log("ERROR: failed to init label for dwd_crowd_meldungen -> error: " + error);
    }
}, false);