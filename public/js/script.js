
try {
    pageTimestampEl = document.querySelector('.repo-latest-release-date');
    if (pageTimestampEl != undefined) {
        pageTimestampEl.style.opacity = 0;
        elDateTime = luxon.DateTime.fromISO(pageTimestampEl.getAttribute("data-timestamp"));
        setTimeout(function () {
            pageTimestampEl.innerHTML = elDateTime.toRelative();
            pageTimestampEl.title = elDateTime.toFormat("DDD t");
            pageTimestampEl.alt = pageTimestampEl.title;
            pageTimestampEl.style.opacity = 1;
        }, 2021);
    }
} catch (error) {
    console.log("ERROR: failed to parse latest release data using luxon.js -> error: " + error)
}

try {
    if (document.querySelectorAll(".toc > ul > li").length > 0) {
        document.querySelectorAll(".toc > ul > li").forEach(function (element) {
            if (element.querySelectorAll("ul > li").length > 0) {
                tocLinkTemp = element.querySelector("a");
                tocLinkTemp.title = "click to toggle childrens";
                tocLinkTemp.setAttribute("aria-roledescription", "click to toggle childrens");

                tocLinkTemp.addEventListener('click', function (event) {
                    event.preventDefault();
                    console.log(event);

                    event.target.parentElement.querySelectorAll("ul").forEach(function (listchild) {
                        //listchild.classList.toggle("hide");
                        console.log(listchild.style.display);
                        if (listchild.style.display.length > 0) {
                            listchild.style.display = listchild.style.display === 'none' ? 'block' : 'none'; // based on: https://stackoverflow.com/a/8454104 - license: CC BY-SA 3.0 - user: Adam Rackis
                        } else {
                            listchild.style.display = 'block';
                        }

                        console.log(listchild.style.display);
                    })
                })
            }
        });
    }
} catch (error) {
    console.log("ERROR: failed to make level 3 toc entries toggle visibility of children items on click -> error: " + e);
}

try {
    var areasTable = document.querySelector("table#areas-table");
    var areasTableRows = document.querySelectorAll("table#areas-table tr");

    if (areasTable != undefined) {
        var filterInput = document.createElement("input");
        filterInput.type = "search";
        filterInput.classList.add("search"); // use: 'fuzzy-search' to switch to fuzzy search mode -> docs: https://listjs.com/docs/fuzzysearch/
        filterInput.placeholder = "Filter the " + (areasTableRows.length - 1) + " table rows ";
        filterInput.id = "areas-table-filter-input";

        //filterInputContainer.appendChild(filterInput);
        //areasTable.before(filterInputContainer);
        areasTable.before(filterInput);

        var options = {
            valueNames: ['warncellID', 'warncenter', 'type', 'name'],
            //searchColumns: ['warncellID', 'name'],
            indexAsync: true,
            searchDelay: 300,
            pagination: [{ // docs: https://listjs.com/docs/pagination/
                innerWindow: 3, // How many pages should be visible on each side of the current page.
                item: '<li class="pagination-item"><a class="page" href="#areas-table"></a></li>'
            }],
            page: 70 // How many elements should be visible per page.            
        };

        var areasList = new List('areas-table-container', options);
        areasList.on('searchComplete', function (event) {
            //console.log(event);
            console.log('DEBUG: search returned ' + areasList.visibleItems.length + ' results');
        });
        /*areasList.on('searchStart',function (event) {
            console.log(event);
        });
        areasList.on('sortStart',function (event) {
            console.log(event);
        });*/

        tableHeads = document.querySelectorAll("table#areas-table thead th");
        for (let index = 0; index < tableHeads.length; index++) {
            try {
                var tableHeadTemp = tableHeads[index];
                tableHeadTemp.addEventListener('click', function (event) {
                    var colNameTemp = event.target.getAttribute("data-sort");
                    var sortOrderTemp = event.target.getAttribute("data-sort-order");

                    areasList.sort(colNameTemp, { order: sortOrderTemp });

                    if (sortOrderTemp == "desc") {
                        event.target.setAttribute("data-sort-order", "asc");
                    } else {
                        event.target.setAttribute("data-sort-order", "desc");
                    }

                    console.log("DEBUG: sorting table by column '" + colNameTemp + "' ");
                });
            } catch (error) {
                console.log("ERROR: failed to init areas table sorting for table head '" + tableHeads[index].innerHTML + "' -> error: " + error);
            }
        }

        /*window.addEventListener("beforeprint", function(event) { // show all table rows when printing
            var filterInput = document.querySelector("#areas-table-filter-input");
            var areaTableRows = document.querySelectorAll(".areas-table-row");
            
            if (filterInput.value.length == 0 || filterInput.value.length == undefined) {
                areasList.show(0, areasList.size()-1);
            }
        });*/
    }
} catch (error) {
    console.log("ERROR: failed to setup areas table filter -> error: " + error);
}

try {
    var stationsTable = document.querySelector("table#stations-table");
    var stationsTableRows = document.querySelectorAll("table#stations-table tr");

    if (stationsTable != undefined) {
        var filterInput = document.createElement("input");
        filterInput.type = "search";
        filterInput.classList.add("search"); // use: 'fuzzy-search' to switch to fuzzy search mode -> docs: https://listjs.com/docs/fuzzysearch/
        filterInput.placeholder = "Filter the " + (stationsTableRows.length - 1) + " table rows ";
        filterInput.id = "stations-table-filter-input";

        //filterInputContainer.appendChild(filterInput);
        //stationsTable.before(filterInputContainer);
        stationsTable.before(filterInput);

        var options = {
            valueNames: ['id', 'name', 'latitude', 'longtitude', 'altitude'],
            //searchColumns: ['id', 'name'],
            indexAsync: true,
            searchDelay: 300,
            pagination: [{ // docs: https://listjs.com/docs/pagination/
                innerWindow: 3, // How many pages should be visible on each side of the current page.
                item: '<li class="pagination-item"><a class="page" href="#stations-table"></a></li>'
            }],
            page: 70 // How many elements should be visible per page.            
        };

        var stationsList = new List('stations-table-container', options);
        stationsList.on('searchComplete', function (event) {
            //console.log(event);
            console.log('DEBUG: search returned ' + stationsList.visibleItems.length + ' results');
        });
        /*stationsList.on('searchStart',function (event) {
            console.log(event);
        });
        stationsList.on('sortStart',function (event) {
            console.log(event);
        });*/

        tableHeads = document.querySelectorAll("table#stations-table thead th");
        for (let index = 0; index < tableHeads.length; index++) {
            try {
                var tableHeadTemp = tableHeads[index];
                tableHeadTemp.addEventListener('click', function (event) {
                    var colNameTemp = event.target.getAttribute("data-sort");
                    var sortOrderTemp = event.target.getAttribute("data-sort-order");

                    stationsList.sort(colNameTemp, { order: sortOrderTemp });

                    if (sortOrderTemp == "desc") {
                        event.target.setAttribute("data-sort-order", "asc");
                    } else {
                        event.target.setAttribute("data-sort-order", "desc");
                    }

                    console.log("DEBUG: sorting table by column '" + colNameTemp + "' ");
                });
            } catch (error) {
                console.log("ERROR: failed to init stations table sorting for table head '" + tableHeads[index].innerHTML + "' -> error: " + error);
            }
        }

        /*window.addEventListener("beforeprint", function(event) { // show all table rows when printing
            var filterInput = document.querySelector("#stations-table-filter-input");
            var stationTableRows = document.querySelectorAll(".stations-table-row");
                
            if (filterInput.value.length == 0 || filterInput.value.length == undefined) {
                stationsList.show(0, stationsList.size()-1);
            }
        });*/
    }
} catch (error) {
    console.log("ERROR: failed to setup stations table filter -> error: " + error);
}