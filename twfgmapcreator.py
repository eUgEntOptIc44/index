"""

@title TinyWeatherForecastGermany - leaflet map featuring data provided by DWD/WMO - GitLab Pages page

@author Jean-Luc Tibaux (https://gitlab.com/eUgEntOptIc44)

@license GPLv3

@since July 2021

No warranty or guarantee of any kind provided. Use at your own risk.
Not meant to be used in commercial or in general critical/productive environments at all.

Copyright of the remixed public data:
DWD, WMO, OpenStreetMap, OpenTopoMap, Stamen

"""

import base64
from datetime import datetime, date, timedelta
from pathlib import Path
from pprint import pprint
import logging
import json
import sys

from bs4 import BeautifulSoup

import folium
import folium.plugins

from dateutil.tz import tzutc # timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
import htmlmin
import humanize # human readable file sizes, timestamps, ...

from webassets import Bundle, Environment
from jsmin import jsmin
from cssmin import cssmin

import requests
import regex

workingDir = Path("public")
workingDir.mkdir(parents=True, exist_ok=True) # create directory if not exists

try:
    logging.basicConfig(format=u'%(asctime)-s %(levelname)s [%(name)s]: %(message)s',
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(str(Path(workingDir / "debug.log").absolute()), encoding="utf-8"),
            logging.StreamHandler()
    ])
except Exception as e:
    logging.error("while logger init! -> error: "+str(e))

foliumMap = folium.Map(location=[50.9, 10.3], tiles='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', name="OpenStreetMap (OSM)", zoom_start=6, control_scale=True, attr='© Warngebiete, WMS-Layers & Stations -> <a href="https://opendata.dwd.de/">DWD</a>/<a href="https://www.wwis.dwd.de/de/home.html">WMO</a> | <a href="https://leafletjs.com/">Leaflet</a> | Map Data by © <a href="http://openstreetmap.org/">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.')

folium.TileLayer(tiles='stamentoner', name='stamen toner').add_to(foliumMap)
logging.debug("added 'stamentoner' tile layer ")

folium.TileLayer(tiles='stamenterrain', name='stamen terrain').add_to(foliumMap)
logging.debug("added 'stamenterrain' tile layer ")

folium.TileLayer(tiles='stamenwatercolor', name='stamen water color').add_to(foliumMap)
logging.debug("added 'stamenwatercolor' tile layer ")

folium.TileLayer(tiles='https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', name="OpenTopoMap", attr="opentopomap").add_to(foliumMap)
logging.debug("added 'opentopomap' tile layer ")

#region stations -> DWD/WMO weather stations

stationsJsonFile = workingDir / "twfg-stations.json"
stationsJsonFileSize = stationsJsonFile.stat().st_size

if stationsJsonFile.exists() != True:
    logging.error(" 'stations' json file '"+str(stationsJsonFile.absolute())+"' does not exist or is not readable!")
    sys.exit("FATAL ERROR script execution aborted!")
elif stationsJsonFileSize < 1:
    logging.error(" size of 'stations' json file '"+str(stationsJsonFile.absolute())+"' -> '"+str(stationsJsonFileSize)+"' is too small!")
    sys.exit("FATAL ERROR script execution aborted!")
else:
    logging.debug("successfully loaded 'stations' json file '"+str(stationsJsonFile.absolute())+"' ("+str(humanize.naturalsize(stationsJsonFileSize, binary=True))+") ")

try:
    with open(str(stationsJsonFile.absolute()), "r", encoding="utf-8") as fh:
        stationsJsonList = json.loads(str(fh.read()))
        logging.debug("parsed "+str(len(stationsJsonList))+" stations")
except Exception as e:
    logging.error("failed to open '"+str(stationsJsonFile.absolute())+"' -> error: "+str(e))
    sys.exit("FATAL ERROR script execution aborted!")

stationCluster = folium.plugins.MarkerCluster(name="DWD/WMO stations").add_to(foliumMap) # docs see: https://python-visualization.github.io/folium/plugins.html#folium.plugins.MarkerCluster

for stationIndex in range(len(stationsJsonList)):
    stationTemp = stationsJsonList[stationIndex]
    folium.CircleMarker(
        location=[stationTemp["latitude"], stationTemp["longtitude"]],
        radius=6,
        popup= folium.Popup(html="<b>"+str(stationTemp["name"])+".</b> <br>"+str(stationTemp["id"])+"<br>"+str(stationTemp["altitude"])+"m ", parse_html=False),
        tooltip=str(stationTemp["name"]),
        color="blue",
        fill=False,
    ).add_to(stationCluster)

logging.debug("completed iteration of stations")

#endregion

#region Warngebiete -> warning areas by DWD

warngebieteNames = ["dwd:Warngebiete_Binnenseen","dwd:Warngebiete_Bundeslaender","dwd:Warngebiete_Gemeinden","dwd:Warngebiete_Kreise","dwd:Warngebiete_Kueste","dwd:Warngebiete_See"]

for warngebieteNameTemp in warngebieteNames:
    try:
        warngebieteRawTemp = requests.get("https://maps.dwd.de/geoserver/dwd/ows?service=WFS&version=1.0.0&request=GetFeature&typeName="+str(warngebieteNameTemp)+"&maxFeatures=20000&OutputFormat=application/json")

        warngebieteRawTemp = str(warngebieteRawTemp.text).strip()

        try:
            warngebieteJsonTemp = json.loads(warngebieteRawTemp)
            logging.debug("retrieved "+str(len(warngebieteJsonTemp["features"]))+" geojson features for warngebiet '"+str(warngebieteNameTemp)+"' ")
        except Exception as e:
            logging.error("parsing of json data for warngebiete '"+str(warngebieteNameTemp)+"' failed -> error: "+str(e))

        with open(str(Path(workingDir / Path("twfg-warngebiete-"+regex.sub(r'(?im)[^A-z0-9]+','_',str(warngebieteNameTemp)).lower()+"-geojson-temp.json")).absolute()), "w+", encoding="utf-8") as fh:
            fh.write(warngebieteRawTemp)

    except Exception as e:
        logging.error("requesting of data for warngebiete '"+str(warngebieteNameTemp)+"' failed -> error: "+str(e))

geojsonFiles = list(workingDir.glob('twfg-warngebiete-*geojson-*.json'))
#pprint(geojsonFiles)

geojsonColors = { # colors for geojson files retrieved from DWD (see above)
    "twfg-warngebiete-dwd_warngebiete_binnenseen-geojson-temp.json":{ # expect ca. 23 geojson features
        "name":"Binnenseen",
        "stroke":"#037CFF",
        "fill":"#037CFF"
    },
    "twfg-warngebiete-dwd_warngebiete_bundeslaender-geojson-temp.json":{ # expect 16 geojson features
        "name":"Bundesländer",
        "stroke":"#656565",
        "fill":"#656565"
    },
    "twfg-warngebiete-dwd_warngebiete_gemeinden-geojson-temp.json":{ # expect ca. 11.176 geojson features
        "name":"Gemeinden",
        "stroke":"rgb(249 134 23 / 73%)", # "stroke":"rgb(249 134 23 / 73%)",
        "fill":"rgb(249 134 23 / 73%)"
    },
    "twfg-warngebiete-dwd_warngebiete_kreise-geojson-temp.json":{ # expect ca. 402 geojson features
        "name":"Kreise",
        "stroke":"rgb(214 112 214 / 76%)", # "stroke":"rgb(214 112 214 / 86%)",
        "fill":"rgb(214 112 214 / 76%)"
    },
    "twfg-warngebiete-dwd_warngebiete_kueste-geojson-temp.json":{ # expect ca. 8 geojson features
        "name":"Küste",
        "stroke":"rgb(53 195 245 / 84%)", # "stroke":"rgb(53 195 245 / 84%)",
        "fill":"rgb(53 195 245 / 84%)"
    },
    "twfg-warngebiete-dwd_warngebiete_see-geojson-temp.json":{ # expect ca. 20 geojson features
        "name":"See",
        "stroke":"rgb(30 124 255 / 84%)", # "stroke":"rgb(30 124 255 / 84%)",
        "fill":"rgb(30 124 255 / 84%)"
    }
}

for geojsonFileTemp in geojsonFiles:
    # iterate through file names and load data from disk
    # instead of memory (->e.g. from a list variable filled above)
    # to reduce memory load

    try:
        #pprint(geojsonColors[str(geojsonFileTemp.name)])
        geojsonFileNameTemp = str(geojsonFileTemp.name).strip()

        fillColorTemp = str(geojsonColors[geojsonFileNameTemp]["fill"])
        strokeColorTemp = str(geojsonColors[geojsonFileNameTemp]["stroke"])

        try:
            dataTemp = str(open(str(geojsonFileTemp.absolute()), encoding="utf-8").read())
        except Exception as e:
            dataTemp = ""
            logging.error("failed to open geojson file '"+str(geojsonFileTemp.absolute())+"' -> error: "+str(e))

        if dataTemp != "":
            showLayerOnStart = False # if False layer is hidden -> can be toggled by user from top-right 'layer' control in UI

            if "see" in geojsonFileNameTemp.lower() or "laender" in geojsonFileNameTemp.lower(): # we had to limit this to reduce memory load
                showLayerOnStart = True

            foliumMap.add_child(folium.GeoJson(data = dataTemp, style_function=lambda x, fillColorTemp=fillColorTemp, strokeColorTemp=strokeColorTemp: {'fillColor': fillColorTemp, 'fillOpacity':'0.1', 'color': strokeColorTemp, 'strokeWidth':'0.75', 'strokeOpacity':'0.5'}, name=geojsonColors[str(geojsonFileTemp.name)]["name"], popup=folium.GeoJsonPopup(['NAME','WARNCELLID','WARNCENTER','MIN_HEIGHT','MAX_HEIGHT']), tooltip=folium.GeoJsonTooltip(fields=['NAME'], labels=False), zoom_on_click=True, smooth_factor=1.1, embed=True, show=showLayerOnStart))
            logging.debug("added map layer '"+geojsonFileNameTemp+"' ")
        else:
            logging.error("failed to add map layer '"+geojsonFileNameTemp+"'")
    except Exception as e:
        logging.error("failed to add map layer '"+geojsonFileNameTemp+"' -> error: "+str(e))

#endregion

#region DWD WMS Map Layers
#  -> folium docs: https://python-visualization.github.io/folium/modules.html#folium.raster_layers.WmsTileLayer
#  -> inspired by: https://github.com/guidocioni/world-weather-map/blob/master/webapp.py
folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD Sat World',
    layers="dwd:SAT_WELT_KOMPOSIT", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.7,
    version='1.3.0',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:SAT_WELT_KOMPOSIT' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD Sat EU (RGB)',
    layers="dwd:SAT_EU_RGB", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.7,
    version='1.3.0',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:SAT_EU_RGB' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD WN radar',
    layers="dwd:WN-Produkt", 
    fmt="image/png",
    show=True,
    transparent=True,
    opacity=0.6,
    version='1.3.0',
    styles='wn-produkt-ohne-abdeckung',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:WN-Produkt' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD Warnungen Bundesländer',
    layers="dwd:Warnungen_Bundeslaender", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.5,
    version='1.3.0',
    #styles='',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:Warnungen_Bundeslaender' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD Warnungen Gemeinden',
    layers="dwd:Warnungen_Gemeinden", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.5,
    version='1.3.0',
    #styles='',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:Warnungen_Gemeinden' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD Warnungen Landkreise',
    layers="dwd:Warnungen_Landkreise", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.5,
    version='1.3.0',
    #styles='',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:Warnungen_Landkreise' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD Warnungen Küste',
    layers="dwd:Warnungen_Kueste", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.5,
    version='1.3.0',
    #styles='',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:Warnungen_Kueste' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD Warnungen Binnenseen',
    layers="dwd:Warnungen_Binnenseen", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.5,
    version='1.3.0',
    #styles='',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:Warnungen_Binnenseen' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD UV Dosis Global CL',
    layers="dwd:UV_Dosis_Global_CL", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.5,
    version='1.3.0',
    #styles='',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:UV_Dosis_Global_CL' WMS tile layer ")

folium.WmsTileLayer(url="https://maps.dwd.de/geoserver/ows?",
    name='DWD UV Dosis EU CL',
    layers="dwd:UV_Dosis_EU_CS", 
    fmt="image/png",
    show=False,
    transparent=True,
    opacity=0.5,
    version='1.3.0',
    #styles='',
    detectRetina=True).add_to(foliumMap)

logging.debug("added 'dwd:UV_Dosis_EU_CS' WMS tile layer ")

#endregion DWD WMS Map Layers

folium.LayerControl().add_to(foliumMap)

#region folium plugins

#folium.plugins.Terminator().add_to(foliumMap) # adds day/night overlay
#foliumMap.add_child(folium.plugins.Draw(export=True, filename='drawing.geojson', position='topleft', draw_options=None, edit_options=None)) # draw and modify geojson on map # docs: http://leaflet.github.io/Leaflet.draw/docs/leaflet-draw-latest.html

foliumMap.add_child(folium.plugins.LocateControl())

foliumMap.add_child(folium.plugins.MiniMap(toggle_display=True, auto_toggle_display=True)) # adds MiniMap to map -> source: https://github.com/Norkart/Leaflet-MiniMap

foliumMap.add_child(folium.plugins.MeasureControl())

# plugin showing mouse coordinates -> docs see: https://python-visualization.github.io/folium/plugins.html#folium.plugins.MousePosition
fmtr = "function(num) {return L.Util.formatNum(num, 3) + ' º ';};"
folium.plugins.MousePosition(position='topright', separator=' | ', prefix="", empty_string="", lat_formatter=fmtr, lng_formatter=fmtr).add_to(foliumMap) # German labels: prefix -> "Koordinaten des Cursors:" # empty_string -> "Keine Position verfügbar."

# docs see: https://python-visualization.github.io/folium/plugins.html#folium.plugins.Fullscreen
folium.plugins.Fullscreen(position='topleft', title='Vollbild', title_cancel='Vollbild beenden', force_separate_button=False).add_to(foliumMap)

#endregion

#foliumMap.save(str(Path(workingDir / "map.html").absolute()))
#sys.exit(0)

logging.debug("rendering folium map ... ")

mapSoup = BeautifulSoup(str(foliumMap.get_root().render()), features='html.parser') # parse html to modify elements # rendering map to html string -> source: https://github.com/python-visualization/folium/issues/781

logging.debug("parsed folium map html data ")

scriptTags = mapSoup.select("head script")

scriptTagsSrcList = []
for scriptTagTemp in scriptTags:
    SrcTemp = str(scriptTagTemp.get('src')).strip().replace("None","")
    if len(SrcTemp) > 4:
        scriptTagsSrcList.append(SrcTemp)
        scriptTagTemp.decompose()

scriptTagsSrcList.append("js/dwd_crowd.js")

#region AQI data by EEA
try:
    headers = {
        'User-Agent': '',
        'DNT':'1',
        'Origin':'https://airindex.eea.europa.eu',
        'Referer':'https://airindex.eea.europa.eu/',
        'Accept-Language':'en-US,en;q=0.5'
    }

    api_base_url = 'https://dis2datalake.blob.core.windows.net/airquality-derivated/AQI/'

    index_req = requests.get(f"{api_base_url}content/index.json", headers=headers)
    logging.debug(f"AQI station data -> Last-Modified: {index_req.headers['Last-Modified']}") # -> e.g. Mon, 02 May 2022 10:20:08 GMT

    index_json = index_req.json()
    #pprint(index_json)

    stations_file = str(index_json['contents'][0])

    stations_req = requests.get(f"{api_base_url}content/{stations_file}", headers=headers)
    stations_file = workingDir / "AQI-stations.json"

    stations_json = stations_req.json()
    #pprint(stations_json)

    # with open(stations_file, 'r', encoding='utf-8') as fh:
    #     stations_json = json.loads(str(fh.read()))

    len_stations = len(stations_json)

    logging.debug(f"received {len_stations} AQI stations from datalake")

    if len_stations > 0:
        with open(stations_file, 'w', encoding='utf-8') as fh:
            fh.write(json.dumps(stations_json, indent=4))
    
        scriptTagsSrcList.append("js/aqi_stations.js") # insert javascript for frontend

        logging.debug(f"succesfully inserted js for AQI stations")
    else:
        logging.debug(f"failed to insert AQI stations -> length of stations is invalid")

except Exception as e:
    logging.debug(f"processing of AQI stations failed -> error: {e} ")
#endregion

#pprint(scriptTagsSrcList)

styleTags = mapSoup.select('head link[rel="stylesheet"]')

styleTagsHrefList = []
for styleTagTemp in styleTags:
    SrcTemp = str(styleTagTemp.get('href')).strip().replace("None","")
    if len(SrcTemp) > 4:
        styleTagsHrefList.append(SrcTemp)
        styleTagTemp.decompose()

#pprint(styleTagsHrefList)

oldjs = list(Path(workingDir / "js").glob('script_map_*.js'))
#pprint(oldjs)

for oldjsTemp in oldjs:
    try:
        oldjsTemp.unlink()
    except Exception as e:
        logging.error("failed to delete '"+str(oldjsTemp.absolute())+"' -> error: "+str(e))

oldcss = list(Path(workingDir / "css").glob('style_map_*.css'))
#pprint(oldcss)

for oldcssTemp in oldcss:
    try:
        oldcssTemp.unlink()
    except Exception as e:
        logging.error("failed to delete '"+str(oldcssTemp.absolute())+"' -> error: "+str(e))

assetsEnv = Environment(directory=str(workingDir.absolute()), url='https://tinyweatherforecastgermanygroup.gitlab.io/index')

js = Bundle(*tuple(scriptTagsSrcList), filters='jsmin', output='js/script_map_%(version)s.min.js') # 
assetsEnv.register('js_map', js)

css = Bundle(*tuple(styleTagsHrefList), filters='cssmin', output='css/style_map_%(version)s.min.css')
assetsEnv.register('css_map', css)

assetsHtmlStr = ""

try:
    jsAssetUrls = list(assetsEnv['js_map'].urls())
    for jsAssetUrlTemp in jsAssetUrls:
        assetsHtmlStr += '<script src="' + str(jsAssetUrlTemp) + '"></script>\n'
except Exception as e:
    logging.error("failed to minify js assets -> error: "+str(e))

try:
    cssAssetUrls = list(assetsEnv['css_map'].urls())
    for cssAssetUrlTemp in cssAssetUrls:
        cssAssetPath = Path("css") / Path(cssAssetUrlTemp.replace(str(assetsEnv.url),'').replace('\\','').replace('/','').replace('css','')+"css")

        with open(str(workingDir / cssAssetPath), "r", encoding="utf-8") as fh:
            cssFileContentsTemp = str(fh.read())

        cssFileContentsTemp = regex.sub(regex.compile(r'(?im)(\.\.\/fonts\/fontawesome\-webfont\.)([a-z0-9]+)(\?[^\'\"]+)'),r'\g<1>\g<2>',cssFileContentsTemp)

        with open(str(workingDir / cssAssetPath), "w+", encoding="utf-8") as fh:
            fh.write(str(cssFileContentsTemp))
        
        cssAssetUrlTemp

        assetsHtmlStr += '<link rel="stylesheet" href="' + str(cssAssetUrlTemp) + '" media="all">\n'
except Exception as e:
    logging.error("failed to minify css assets -> error: "+str(e))

try:
    mapSoup.select("meta")[0].insert_after(BeautifulSoup(str(assetsHtmlStr), features='html.parser')) # parse html to modify elements
except Exception as e:
    logging.error("failed to insert js and css assets into html rendering result -> error: "+str(e))

try:
    if len(mapSoup.select("head title")) > 0:
        mapSoup.select("head title")[0].string = "map - Tiny Weather Forecast Germany"
    else:
        mapSoup.select("meta")[0].insert_after(BeautifulSoup(str('<title>map - Tiny Weather Forecast Germany</title>'), features='html.parser')) # parse html to modify elements
except Exception as e:
    logging.error("failed to set head -> target: 'head title' -> error: "+str(e))

try:
    meta_tags = list(mapSoup.select("head meta"))
    if len(meta_tags) > 0:
        for tag in meta_tags:
            try:
                if tag.content != None:
                    meta_tags.content = regex.sub(r'(?im)[\n\r\t]+',' ',str(tag.string))
            except Exception as e:
                logging.error("failed to clean meta tag -> target: '"+str(tag)+"' -> error: "+str(e))
except Exception as e:
    logging.error("failed to clean meta tags -> target: 'head meta' -> error: "+str(e))

try:
    inline_js = list(mapSoup.select("script"))
    for tag in inline_js:
        try:
            if tag.string != None:
                tag.string = jsmin(str(tag.string))
        except Exception as e:
            logging.error("failed to minify in-line js tag -> error: "+str(e))

except Exception as e:
    logging.error("failed to minify in-line js -> target: 'script' -> error: "+str(e))

try:
    inline_css = list(mapSoup.select("style"))
    for tag in inline_css:
        try:
            if tag.string != None:
                tag.string = cssmin(str(tag.string))

                tag.string = regex.sub(r'(?m)[\.\#]*\w+{}','',str(tag.string)) # remove empty tags
        except Exception as e:
            logging.error("failed to minify in-line css tag -> error: "+str(e))

except Exception as e:
    logging.error("failed to minify in-line css -> target: 'style' -> error: "+str(e))

mapSoup = str(mapSoup)

mapSoup = regex.sub(r'(?m)(base_layers[^\:]*\:[\{]*\{)(\")(https\:\/\/\{s\}\.tile\.openstreetmap\.org\/\{z\}\/\{x\}\/\{y\}\.png)(\")','\g<1>"OpenStreetMap"',str(mapSoup))

mapSoup = regex.sub(r'(?m)(map\_)([A-z0-9]{32})','twfg_folium_map',str(mapSoup))

mapHtmlFile = str(Path(workingDir / "map.html").absolute())
try:
    with open(mapHtmlFile, "w+", encoding="utf-8") as fh:
        fh.write(htmlmin.minify(mapSoup, remove_empty_space=True))
except Exception as e:
    logging.error("minification of '"+mapHtmlFile+"' failed -> error: "+str(e))
    with open(mapHtmlFile, "w+", encoding="utf-8") as fh:
        fh.write(mapSoup)

print("done")
